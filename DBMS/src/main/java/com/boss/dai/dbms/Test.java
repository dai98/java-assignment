package com.boss.dai.dbms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author 戴若汐
 */
@Component
@Slf4j
public class Test {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Object createTable(final int userNum, final int roleNum){
        return jdbcTemplate.execute(new CallableStatementCreator(){
            @Override
            public CallableStatement createCallableStatement(Connection conn) throws SQLException {
                String sql = "{call proc_init_base_data6(?,?)}";
                try{
                    CallableStatement cs = conn.prepareCall(sql);
                    cs.setInt(1,userNum);
                    cs.setInt(2,roleNum);
                    return cs;
                }catch(Exception e){
                    log.error(e.toString());
                }finally{
                    createCallableStatement(conn).close();
                }
                return null;
            }
            },
                new CallableStatementCallback(){
                    @Override
                    public Object doInCallableStatement(CallableStatement cs) throws SQLException{
                        cs.execute();
                        return null;
                    }
                }
        );
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Test test = context.getBean(Test.class);
        test.createTable(100000,10);
    }
}
