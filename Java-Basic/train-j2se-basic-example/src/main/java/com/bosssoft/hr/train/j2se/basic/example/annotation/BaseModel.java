package com.bosssoft.hr.train.j2se.basic.example.annotation;

import com.bosssoft.hr.train.j2se.basic.example.util.DBUtil;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 戴若汐
 */
@Slf4j
public class BaseModel {

    /**
     * 根据子类的注解生成 Insert into xxx (xx1, xx2, xx3) values (?,?,?)
     * 并调用jdbc完成记录保存
     * @return 影响的行数
     */
    public int save() throws IllegalAccessException, InvocationTargetException, ClassNotFoundException{
        // 获取@Table注释的值 来获取表的名称
        Table tableAnnotation = this.getClass().getAnnotation(Table.class);
        String tableValue = tableAnnotation.value();
        Field[] fields = this.getClass().getDeclaredFields();
        // 通过反射来获取字段的值
        StringBuilder attributeFields = new StringBuilder("(");
        StringBuilder attributeValues = new StringBuilder("(");
        for(Field field: fields){
            Annotation annotation = field.getDeclaredAnnotations()[0];
            Method method = annotation.annotationType().getDeclaredMethods()[0];
            method.setAccessible(true);
            field.setAccessible(true);
            String result = method.invoke(annotation).toString();
            attributeFields.append((result == null) ? "NULL" : result).append(",");
            attributeValues.append(field.get(this)).append(",");
        }

        // 生成SQL语句
        String attribute = attributeFields.toString();
        attribute = attribute.substring(0,attribute.length()-2) + ")";
        String value = attributeValues.toString();
        value = value.substring(0,value.length()-2) + ")";
        String sql = "INSERT INTO " + tableValue + " " + attribute + " VALUES " + value;
        log.info(sql);
        return DBUtil.executeUpdate(sql);
    }

    /**
     * 要求根据子类的注解生成 update xxx set xx1=?,xx2=? … where id=?
     * 并且调用 jdbc 完成记录保存
     * @return 影响的行数
     */
    public int update() throws IllegalAccessException, InvocationTargetException{
        StringBuilder sb = new StringBuilder("UPDATE ");

        Table tableAnnotation = this.getClass().getAnnotation(Table.class);
        String tableValue = tableAnnotation.value();
        sb.append(tableValue).append(" SET ");
        Field[] fields = this.getClass().getDeclaredFields();
        Object id = new Object();
        for(Field field: fields){
            Annotation annotation = field.getDeclaredAnnotations()[0];
            Method method = annotation.annotationType().getDeclaredMethods()[0];
            method.setAccessible(true);
            field.setAccessible(true);
            Object result = method.invoke(annotation);
            sb.append((result == null) ? "NULL" : result).append("=");
            sb.append(field.get(this)).append(",");
            if ("ID".equals(field.getName())){
                id = field.get(this);
            }
        }
        sb.deleteCharAt(sb.length() - 1).append(" WHERE id=").append(id);
        String sql = sb.toString();
        log.info(sql);
        return DBUtil.executeUpdate(sql);
    }

    /**
     * 要求根据子类的注解生成 delete from xxx where id=?
     * 并且调用 jdbc 完成记录保存
     * @return 影响的行数
     */
    public int remove() throws IllegalAccessException{
        StringBuilder sb = new StringBuilder("DELETE FROM ");

        Table tableAnnotation = this.getClass().getAnnotation(Table.class);
        String tableValue = tableAnnotation.value();
        sb.append(tableValue).append(" WHERE id=");
        Field[] fields = this.getClass().getDeclaredFields();
        Object id = new Object();
        for(Field field: fields){
            field.setAccessible(true);
            if ("ID".equals(field.getName())){
                id = field.get(this);
            }
        }
        String sql = sb.append(id).toString();
        log.info(sql);

        return DBUtil.executeUpdate(sql);
    }

    /**
     * 查询返回对象列表
     * @return 查询结果
     */
    public List<Object> queryForList() throws SQLException {
        Table tableAnnotation = this.getClass().getAnnotation(Table.class);
        String tableValue = tableAnnotation.value();
        String sql = "select * from " + tableValue;
        ResultSet rs = DBUtil.query(sql);
        List<Object> result = new ArrayList<>();
        ResultSetMetaData md;
        //获得结果集结构信息,元数据
        if (rs != null){
            md = rs.getMetaData();
        }else{
            return new ArrayList<>();
        }

        //获得列数
        int columnCount = md.getColumnCount();

        while (rs.next()) {
            Map<String, Object> rowData = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                rowData.put(md.getColumnName(i), rs.getObject(i));
            }
            result.add(rowData);
        }
        log.info(sql);
        return result;
    }
}
