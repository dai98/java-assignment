package com.bosssoft.hr.train.j2se.basic.example.annotation;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table(value = "t_user")
public class UserModel extends BaseModel
{
    @Id(value = "id")
    private long id;

    @Column(value="name")
    private String name;

    @Column(value="age")
    private Integer age;

    public UserModel(long id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
