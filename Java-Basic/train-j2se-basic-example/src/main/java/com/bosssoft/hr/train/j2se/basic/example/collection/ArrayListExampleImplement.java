package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * @author 戴若汐
 */
@Slf4j
public class ArrayListExampleImplement implements ArrayListExmaple<User> {

    ArrayList<User> arrayList = new ArrayList<>();
    /**
     * @param position 插入位置
     * @param node 插入元素
     * @return 是否插入成功
     */
    @Override
    public boolean insert(Integer position, User node) {
        if (position >= 0 && position < arrayList.size() || arrayList.isEmpty()){
            arrayList.add(position, node);
            return true;
        }else{
            return false;
        }
    }

    /**
     * 加入尾部
     * @param node 新增元素
     * @return 是否添加成功
     */
    @Override
    public boolean append(User node) {
        return arrayList.add(node);
    }

    /**
     * 返回指定位置的元素
     *
     * @param index 索引
     * @return 元素
     */
    @Override
    public User get(Integer index) {
        return (index >= 0 && index < arrayList.size()) ? arrayList.get(index) : null;
    }

    /**
     * 中间删除
     *
     * @param position 删除指定位置上的元素
     * @return
     */
    @Override
    public boolean remove(Integer position) {
        return (position >= 0 && position < arrayList.size()) && (arrayList.remove(position.intValue())!=null);
    }

    /**
     * 按下表遍历 注意这种方式遍历过程不可以删除
     */
    @Override
    public void listByIndex() {
        int size = arrayList.size();
        for(int i=0;i<size;i++){
            log.info(Constraint.LOG_TAG + "   " + arrayList.get(i));
        }
    }

    /**
     * 按迭代器遍历
     */
    @Override
    public void listByIterator() {
        Iterator<User> iterator = arrayList.iterator();
        User user = null;
        for(;iterator.hasNext();user = iterator.next()){
            log.info(Constraint.LOG_TAG + "   " + user);
        }
    }

    /**
     * 列表数组转换
     *
     * @return
     */
    @Override
    public User[] toArray() {
        if (arrayList == null){
            return new User[0];
        }
        User[] users = new User[arrayList.size()];
        for(int i=0;i<arrayList.size();i++){
            users[i] = arrayList.get(i);
        }
        return users;
    }

    /**
     * 列表排序演示
     */
    @Override
    public void sort() {
        arrayList.sort(Comparator.comparingInt(User::getId));
    }
}
