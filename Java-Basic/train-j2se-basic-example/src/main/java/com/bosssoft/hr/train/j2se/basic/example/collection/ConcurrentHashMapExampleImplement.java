package com.bosssoft.hr.train.j2se.basic.example.collection;

import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
/**
 * @author: 戴若汐
 */
public class ConcurrentHashMapExampleImplement extends MapImplement{
    public ConcurrentHashMapExampleImplement(){
        super();
        super.setMap(new ConcurrentHashMap<>());
    }
}
