package com.bosssoft.hr.train.j2se.basic.example.collection;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author: 戴若汐
 */
@Slf4j
public class HashMapExampleImplement extends MapImplement {
    public HashMapExampleImplement(){
        super();
        super.setMap(new HashMap<>());
    }
}
