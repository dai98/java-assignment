package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author 戴若汐
 */
@Slf4j
public class LinkedListExampleImplement implements LinkedListExmaple<User> {

    private LinkedList<User> linkedList = new LinkedList<>();
    /**
     * 添加到链表头
     *
     * @param node
     */
    @Override
    public void addFirst(User node) {
        linkedList.addFirst(node);
    }

    public int size(){
        return linkedList.size();
    }

    /**
     * 添加到链表尾
     *
     * @param node
     */
    @Override
    public boolean offer(User node) {
        return linkedList.offer(node);
    }

    /**
     * 设置同步访问防止多线程并发的安全问题
     *
     * @param node
     */
    @Override
    public void sychronizedVisit(User node) {
        Collections.synchronizedCollection(linkedList).add(node);
    }

    /**
     * ===============================>链表因为实现queue接口可以当做队列
     *
     * @param node
     */
    @Override
    public void push(User node) {
        linkedList.push(node);
    }

    /**
     * 出队
     *
     * @return
     */
    @Override
    public User pop() {
        return linkedList.pop();
    }

    /**
     * 加入尾部
     *
     * @param user
     * @return
     */
    @Override
    public boolean append(User user) {
        return linkedList.add(user);
    }

    /**
     * 返回指定位置
     *
     * @param index
     * @return
     */
    @Override
    public User get(Integer index) {
        return (index >= 0 && index < linkedList.size())? linkedList.get(index):null;
    }

    /**
     * 中间删除
     *
     * @param position
     * @return
     */
    @Override
    public boolean remove(Integer position) {
        return (position >= 0 && position < linkedList.size()) && (linkedList.remove(position.intValue())!=null);
    }

    /**
     * 按下表遍历 注意这种方式遍历过程不可以删除
     */
    @Override
    public void listByIndex() {
        int size = linkedList.size();
        for(int i=0;i<size;i++){
            log.info(Constraint.LOG_TAG +"   "+ linkedList.get(i));
        }
    }

    /**
     * 按迭代器遍历
     */
    @Override
    public void listByIterator() {
        Iterator<User> iterator = linkedList.iterator();
        User user = null;
        for(;iterator.hasNext();user = iterator.next()){
            log.info(Constraint.LOG_TAG +"   "+ user);
        }
    }

    /**
     * 列表数组转换
     *
     * @return
     */
    @Override
    public User[] toArray() {
        if (linkedList == null){
            return new User[0];
        }
        User[] users = new User[linkedList.size()];
        for(int i=0;i<linkedList.size();i++){
            users[i] = linkedList.get(i);
        }
        return users;
    }

    /**
     * 列表排序演示
     */
    @Override
    public void sort() {
        linkedList.sort(Comparator.comparingInt(User::getId));
    }
}
