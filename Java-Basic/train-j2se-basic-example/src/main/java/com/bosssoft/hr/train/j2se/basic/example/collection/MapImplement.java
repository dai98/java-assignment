package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author 戴若汐
 */
@Slf4j
public class MapImplement implements HashMapExample{

    private Map<Role,Resource> map;

    public void setMap(Map<Role, Resource> map) {
        this.map = map;
    }

    /**
     * @param key   map的键
     * @param value 值
     * @return 返回值
     */
    @Override
    public Resource put(Role key, Resource value) {
        // null值 null键
        return map.put(key,value);
    }

    /**
     * @param key map的键
     * @return 返回值
     */
    @Override
    public Resource remove(Role key) {
        return map.remove(key);
    }

    @Override
    public String toString(){
        return map.toString();
    }
    /**
     * @param key map的键
     * @return 返回值 是否存在的判断
     */
    @Override
    public boolean containsKey(Role key) {
        return map.containsKey(key);
    }

    /**
     * 迭代方式1
     */
    @Override
    public void visitByEntryset() {
        Set<Map.Entry<Role, Resource>> entrySet = map.entrySet();
        for (Map.Entry<Role,Resource> entry:entrySet){
            log.info(Constraint.LOG_TAG + "   " + entry.getKey() + " " + entry.getValue());
        }
    }

    /**
     * 迭代方式2
     */
    @Override
    public void visitByKeyset() {
        if (null != map){
            Set<Role> set = map.keySet();
            Iterator<Role> keyIter = set.iterator();
            Role role = keyIter.next();
            for(;keyIter.hasNext();role = keyIter.next()){
                log.info(Constraint.LOG_TAG + "   " + role + " " + map.get(role));
            }
        }
    }

    /**
     * 迭代方式3
     */
    @Override
    public void visitByValues() {
        if (null != map){
            Collection<Resource> collection = map.values();
            Iterator<Resource> valueIter = collection.iterator();
            Resource resource = valueIter.next();
            for(;valueIter.hasNext();resource = valueIter.next()){
                log.info(Constraint.LOG_TAG + "   "+ resource);
            }
        }
    }

    public int size(){
        return map.size();
    }
}
