package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author: 戴若汐
 */
@Slf4j
public class QueueExampleImplement implements QueueExmaple<User> {

    private Queue<User> queue = new LinkedList<>();

    @Override
    public boolean add(User e) {
        return queue.add(e);
    }

    @Override
    public boolean remove() {
        return queue.remove(queue.peek());
    }

    @Override
    public boolean offer(User e) {
        return queue.offer(e);
    }

    @Override
    public User poll() {
        return queue.poll();
    }

    @Override
    public User peek() {
        return queue.peek();
    }

    public int size(){
        return queue.size();
    }
}
