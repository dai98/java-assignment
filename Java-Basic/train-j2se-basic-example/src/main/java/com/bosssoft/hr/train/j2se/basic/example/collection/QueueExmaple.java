package com.bosssoft.hr.train.j2se.basic.example.collection;

public interface QueueExmaple<T> {

    boolean add(T e);
    boolean remove();
    boolean offer(T e);
    T poll();
    T peek();
}
