package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author 戴若汐
 */
@Slf4j
public class TreeMapExampleImplement extends MapImplement{
    public TreeMapExampleImplement(){
        super();
        super.setMap(new TreeMap<>(Comparator.comparingInt(Role::getId)));
    }
}