package com.bosssoft.hr.train.j2se.basic.example.pojo;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:07
 * @since
 **/
public class Student extends User {
    private int age;

    public Student(int age) {
        this.age = age;
    }

    public Student(int id, String name, int age) {
        super(id, name);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        if ((obj == null) || (obj.getClass() != this.getClass())){
            return false;
        }
        Student sobj = (Student) obj;
        return sobj.getId().equals(this.getId());
    }

    @Override
    public int hashCode(){
        int hash = 17;
        hash = hash * 31 + getName().hashCode();
        hash = hash * 31 + getAge();
        return hash;
    }
}
