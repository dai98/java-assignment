package com.bosssoft.hr.train.j2se.basic.example.proxy;

/**
 * @author 戴若汐
 */
public interface ByeInterface {
    void sayBye();
}
