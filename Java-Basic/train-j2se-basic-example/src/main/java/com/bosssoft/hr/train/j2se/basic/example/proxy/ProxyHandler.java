package com.bosssoft.hr.train.j2se.basic.example.proxy;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author 戴若汐
 */
@Slf4j
public class ProxyHandler implements InvocationHandler {

    private Object object;
    public ProxyHandler(Object object){
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("Before invoke "  + method.getName());
        method.invoke(object, args);
        log.info("After invoke " + method.getName());
        return null;
    }
}
