package com.bosssoft.hr.train.j2se.basic.example.socket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;

/**
 * @param
 * @author: 戴若汐
 * @create: 2020-05-28 22:25
 * @since
 **/
@Slf4j
public class ClientSocket implements Starter{

    private InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost",8001);

    /**
     * 发送请求数据
     */
    public void send(String requestData){
        SocketChannel socketChannel = null;
        try{
            // 打开通道
            socketChannel = SocketChannel.open(inetSocketAddress);
            // 设置为非阻塞
            socketChannel.configureBlocking(false);
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            socketChannel.write(ByteBuffer.wrap(requestData.getBytes()));
            while (true){
                byteBuffer.clear();
                int readBytes = socketChannel.read(byteBuffer);
                if (readBytes > 0){
                    byteBuffer.flip();
                    log.info("From Server:" + new String(byteBuffer.array(),0,readBytes));
                    socketChannel.close();
                    break;
                }
            }
        }catch(IOException e){
            log.info(e.toString());
        }finally {
            try{
                if(socketChannel != null){
                    socketChannel.close();
                }
            }catch (IOException e){
                log.info(e.toString());
            }

        }

    }

    @Override
    public boolean start() {
        String requestData = "123";
        send(requestData);
        return true;
    }
}
