package com.bosssoft.hr.train.j2se.basic.example.socket;

import lombok.extern.slf4j.Slf4j;

/**
 * @param
 * @description
 * @author Administrator
 * @create 2020-05-28 22:17
 * @since
 **/
@Slf4j
public class ClientSocketApplication {
    public static  void main(String[] args){
        Starter clientSocket=new ClientSocket();
        log.info("--发送信息--");
        clientSocket.start();
    }
}
