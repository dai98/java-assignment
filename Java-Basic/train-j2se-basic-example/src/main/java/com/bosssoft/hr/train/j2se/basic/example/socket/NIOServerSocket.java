package com.bosssoft.hr.train.j2se.basic.example.socket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/
@Slf4j
public class NIOServerSocket implements Starter {

    private Selector selector;
    private ByteBuffer readBuffer = ByteBuffer.allocate(1024);
    private ByteBuffer sendBuffer = ByteBuffer.allocate(1024);
    String str;

    @Override
    public boolean start(){
        // 连接服务器
        ServerSocketChannel socketChannel = null;
        try {
            // 打开通道
            socketChannel = ServerSocketChannel.open();
            // 设置为非阻塞
            socketChannel.configureBlocking(false);
            // 绑定服务
            socketChannel.bind(new InetSocketAddress("localhost", 8001));

            selector = Selector.open();

            // 注册并等待连接
            socketChannel.register(selector, SelectionKey.OP_ACCEPT);

            while (!Thread.currentThread().isInterrupted()) {
                selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> keyIterator = keys.iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isAcceptable()) {
                        accept(key);
                    } else if (key.isReadable()) {
                        read(key);
                    } else if (key.isWritable()) {
                        write(key);
                    }
                    keyIterator.remove(); //该事件已经处理，可以丢弃
                }
            }
        }catch(IOException e){
            log.warn(e.toString());
            return false;
        }finally {
            try{
                if(socketChannel != null){
                    socketChannel.close();
                }
                socketChannel.close();
            }catch (IOException e){
                log.warn(e.toString());
            }

        }
        return true;
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        String message = "Hello Client";
        log.info("Sent to Client: " + message);

        sendBuffer.clear();
        if (message.equals(str)){
            sendBuffer.put(message.getBytes());
        }
        sendBuffer.flip();
        channel.write(sendBuffer);
        channel.register(selector, SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // 清空缓存
        this.readBuffer.clear();
        // 读通道
        int numRead;
        try {
            numRead = socketChannel.read(this.readBuffer);
        } catch (IOException e) {
            // 关闭通道
            key.cancel();
            socketChannel.close();
            return;
        }
        if (numRead != -1) {
            str = new String(readBuffer.array(), 0, numRead);
            log.info("From Client:" + str);
            socketChannel.register(selector, SelectionKey.OP_WRITE);
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel clientChannel = ssc.accept();
        clientChannel.configureBlocking(false);
        clientChannel.register(selector, SelectionKey.OP_READ);
        log.info("a new client connected " + clientChannel.getRemoteAddress());
    }

}