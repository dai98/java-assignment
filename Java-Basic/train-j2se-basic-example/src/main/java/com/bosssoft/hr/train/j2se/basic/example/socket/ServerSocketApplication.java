package com.bosssoft.hr.train.j2se.basic.example.socket;

import lombok.extern.slf4j.Slf4j;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:16
 * @since
 **/
@Slf4j
public class ServerSocketApplication {
    public static  void main(String[] args){
        Starter serverSocket=new NIOServerSocket();
        log.info("--服务器启动--");
        serverSocket.start();
    }
}
