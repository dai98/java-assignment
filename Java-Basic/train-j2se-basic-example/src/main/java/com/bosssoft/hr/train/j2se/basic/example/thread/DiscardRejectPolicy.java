package com.bosssoft.hr.train.j2se.basic.example.thread;

import lombok.extern.slf4j.Slf4j;

/**
 * 丢弃当前线程
 * @author 戴若汐
 */
@Slf4j
public class DiscardRejectPolicy implements RejectPolicy {

    @Override
    public void reject(Runnable task, ThreadPool threadPool) {
        log.warn("丢弃当前线程");
    }
}
