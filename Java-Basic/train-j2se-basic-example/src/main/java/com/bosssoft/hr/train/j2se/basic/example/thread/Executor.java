package com.bosssoft.hr.train.j2se.basic.example.thread;

/**
   线程池的接口
   @author 戴若汐
 */
public interface Executor {

    /**
     *
     * @param command 实现run方法后的线程对象
     */
    void execute(Runnable command);
}
