package com.bosssoft.hr.train.j2se.basic.example.thread;

/**
 * 拒绝策略
 * @author 戴若汐
 */
public interface RejectPolicy {
    /**
     * 拒绝线程运行
     * @param task 线程
     * @param threadPool 线程池
     */
    void reject(Runnable task, ThreadPool threadPool);
}
