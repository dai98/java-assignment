package com.bosssoft.hr.train.j2se.basic.example.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程池
 * @author 戴若汐
 */
@Slf4j
public class ThreadPool implements Executor{

    /**
     * 线程池名称
     */
    private String name;

    /**
     * 核心线程数
     */
    private int coreSize;

    /**
     * 最大线程数
     */
    private int maxSize;

    /**
     * 任务队列
     */
    private BlockingQueue<Runnable> taskQueue;

    /**
     * 拒绝策略 策略模式
     */
    private RejectPolicy rejectPolicy;

    /**
     * 线程序列号
     */
    private AtomicInteger sequence = new AtomicInteger(0);

    /**
     * 当前正在运行的线程数
     * 需要修改时线程间立刻感知 所以使用AtomicInteger
     */
    private AtomicInteger runningCount = new AtomicInteger(0);

    public ThreadPool(String name, int coreSize, int maxSize, BlockingQueue<Runnable> taskQueue, RejectPolicy rejectPolicy) {
        this.name = name;
        this.coreSize = coreSize;
        this.maxSize = maxSize;
        this.taskQueue = taskQueue;
        this.rejectPolicy = rejectPolicy;
    }

    /**
     * 实现方法
     * @param task 实现run方法后的线程对象
     */
    @Override
    public void execute(Runnable task) {
        // 正在运行的线程数
        int count = runningCount.get();
        // 如果正在运行的线程数小于核心线程数 直接添加一个线程
        if (count < coreSize && addWorker(task, true)){
            // 不一定添加成功 addWorker方法里面还要判断一次是不是确实小
            // 如果添加核心线程失败
            return;
        }

        // 如果到达了核心线程数上限 先让任务入队
        // offer方法会在队列满了之后返回false
        if (taskQueue.offer(task)){
            // 什么也不做
        } else{
            // 入队失败了 说明队列满了 添加一个非核心线程
            if (!addWorker(task, false)){
                // 如果添加核心线程依旧失败 那么执行拒绝策略
                rejectPolicy.reject(task, this);
            }
        }
    }

    private boolean addWorker(Runnable newTask, boolean isCore){
        // 自旋判断是不是真的可以创建一个线程
        for(;;){
            int count = runningCount.get();
            // 是否是核心线程
            int max = isCore ? coreSize : maxSize;
            // 不满足创建线程的条件 直接返回false
            if (count >= max){
                return false;
            }

            // 修改runningCount成功 可以创建线程
            if (runningCount.compareAndSet(count, count+1)){
                // 线程的名字
                String threadName = (isCore ? "core_" : "") + name + sequence.incrementAndGet();
                new Thread(() -> {
                    log.info("Thread name: " + Thread.currentThread().getName());
                    Runnable task = newTask;
                    // 不断从任务队列中执行任务 如果取出来的任务为null 则跳出循环 线程结束
                    while (true){
                        try {
                            if (!(task != null || (task = getTask()) != null)) {
                                break;
                            }
                        } catch (InterruptedException e) {
                            log.warn(e.getMessage());
                            Thread.currentThread().interrupt();
                        }
                        try{
                            // 执行任务
                           task.run();
                        } finally {
                            // 任务执行完成 置为空
                            task = null;
                        }
                    }
                }, threadName).start();

                break;
            }
        }
        return true;
    }

    private Runnable getTask() throws InterruptedException {
        try{
            // take()方法会一直阻塞直到取到任务为止
            return taskQueue.take();
        } catch(InterruptedException e){
            // 线程中断了 返回Null结束当前线程
            runningCount.decrementAndGet();
            Thread.currentThread().interrupt();
        }
        return null;
    }

}
