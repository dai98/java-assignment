package com.bosssoft.hr.train.j2se.basic.example.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;

/**
 * @description:  我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
@Slf4j
public final class DBUtil {
    private DBUtil(){ }

    public static int executeUpdate(String sql){
        Connection connection = null;
        Statement statement = null;
        try{
            connection = DbManager.getConnection();
            statement = connection.createStatement();
            return statement.executeUpdate(sql);
        }catch(Exception e){
            log.info(e.toString());
        } finally {
            DbManager.closeAll(connection, statement);
        }
        return 0;
    }

    public static ResultSet query(String sql){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = DbManager.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            return resultSet;
        }catch (SQLException e){
            log.info(e.toString());
        }finally {
            DbManager.closeAll(connection,statement,resultSet);
        }
        return null;
    }
}

@Slf4j
class DbManager {
    private DbManager() {
    }

    private static final String DB_URL = "jdbc:mysql://localhost:3306/j1904?useUnicode=true&characterEncoding=utf-8";
    private static final String USER = "root";

    public static Connection getConnection() throws SQLException {
        return DriverManager
                .getConnection(DB_URL, USER, "123456");
    }

    public static void closeAll(Connection connection, Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            log.info(e.toString());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.info(e.toString());
            }
        }
    }

    public static void closeAll(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            log.info(e.toString());
        } finally {
            closeAll(connection, statement);
        }
    }
}