package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;


/**
 * @author 戴若汐
 */
@Slf4j
public class SAXOperation implements XMLOperation<Student>{

    private String feature = "http://apache.org/xml/features/disallow-doctype-decl";

    @Override
    public boolean create(Student object){
        return false;
    }

    @Override
    public boolean remove(Student object) {
        return false;
    }

    @Override
    public boolean update(Student object) {
        return false;
    }

    @Override
    public Student query(Student object){
        try{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setFeature(feature, true);
            SAXParser parser = factory.newSAXParser();
            parser.parse(new File("src/main/java/com/bosssoft/hr/train/j2se/basic/example/xml/student.xml"), new SAXHandler());
        }catch (Exception e){
            log.info(e.toString());
        }
        return null;
    }

}

@Slf4j
class SAXHandler extends DefaultHandler {

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes){
        log.info("<" + qName + ">");
    }

    @Override
    public void endElement(String uri, String localName, String qName){
        log.info("</" + qName + ">");
    }

    @Override
    public void characters(char[] ch, int start, int length){
        String str = new String(ch, start, length);
        if (!"\n".equals(str)) {
            log.info(str);
        }
    }
}