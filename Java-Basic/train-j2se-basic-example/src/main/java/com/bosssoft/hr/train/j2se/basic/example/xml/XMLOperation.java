package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;

import javax.xml.parsers.ParserConfigurationException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:06
 * @since
 **/
public interface XMLOperation<T extends Student> {
    boolean create(T object) throws ParserConfigurationException;
    boolean remove(T object) throws ParserConfigurationException;
    boolean update(T object) throws ParserConfigurationException;
    T query(T object) throws ParserConfigurationException;

}
