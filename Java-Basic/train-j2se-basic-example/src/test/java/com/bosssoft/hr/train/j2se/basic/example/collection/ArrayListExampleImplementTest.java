package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayListExampleImplementTest {

    @Test
    public void insert() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.insert(0,new User(1,"A"));
        arrayListExampleImplement.insert(0,new User(2,"B"));
        arrayListExampleImplement.insert(0,new User(3,"C"));
        System.out.println(arrayListExampleImplement.arrayList.toString());
        assertEquals(3,arrayListExampleImplement.arrayList.size());
    }

    @Test
    public void append() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        System.out.println(arrayListExampleImplement.arrayList.toString());
        assertEquals(3,arrayListExampleImplement.arrayList.size());
    }

    @Test
    public void get() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        System.out.println(arrayListExampleImplement.arrayList.toString());
        assertEquals(new User(2,"B"),arrayListExampleImplement.get(1));
    }

    @Test
    public void remove() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        System.out.println(arrayListExampleImplement.arrayList.toString());
        arrayListExampleImplement.remove(1);
        System.out.println(arrayListExampleImplement.arrayList.toString());
        assertEquals(2,arrayListExampleImplement.arrayList.size());
    }

    @Test
    public void listByIndex() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        arrayListExampleImplement.listByIndex();
    }

    @Test
    public void listByIterator() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        arrayListExampleImplement.listByIterator();
    }

    @Test
    public void toArray() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        System.out.println(arrayListExampleImplement.toArray());
    }

    @Test
    public void sort() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(2,"A"));
        arrayListExampleImplement.append(new User(1,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        System.out.println(arrayListExampleImplement.arrayList.toString());
        arrayListExampleImplement.sort();
        System.out.println(arrayListExampleImplement.arrayList.toString());
    }
}