package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConcurrentHashMapExampleImplementTest {

    @Test
    public void put() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertEquals(1,concurrentHashMapExampleImplement.size());
    }

    @Test
    public void remove() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertEquals(1,concurrentHashMapExampleImplement.size());
        System.out.println(concurrentHashMapExampleImplement.toString());
        concurrentHashMapExampleImplement.remove(new Role(1,"A"));
        assertEquals(0,concurrentHashMapExampleImplement.size());
    }

    @Test
    public void containsKey() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertTrue(concurrentHashMapExampleImplement.containsKey(new Role(1,"A")));
    }

    @Test
    public void visitByEntryset() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        concurrentHashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        concurrentHashMapExampleImplement.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        concurrentHashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        concurrentHashMapExampleImplement.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        ConcurrentHashMapExampleImplement concurrentHashMapExampleImplement = new ConcurrentHashMapExampleImplement();
        concurrentHashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        concurrentHashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        concurrentHashMapExampleImplement.visitByKeyset();
    }
}