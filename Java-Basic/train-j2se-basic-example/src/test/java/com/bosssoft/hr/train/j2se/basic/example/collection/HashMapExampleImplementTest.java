package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapExampleImplementTest {

    @Test
    public void put() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        System.out.println(hashMapExampleImplement.toString());
        assertEquals(1,hashMapExampleImplement.size());
    }

    @Test
    public void remove() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertEquals(1,hashMapExampleImplement.size());
        hashMapExampleImplement.remove(new Role(1,"A"));
        assertEquals(0,hashMapExampleImplement.size());
    }

    @Test
    public void containsKey() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertTrue(hashMapExampleImplement.containsKey(new Role(1,"A")));
    }

    @Test
    public void visitByEntryset() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        hashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        hashMapExampleImplement.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        hashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        hashMapExampleImplement.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        HashMapExampleImplement hashMapExampleImplement = new HashMapExampleImplement();
        hashMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        hashMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        hashMapExampleImplement.visitByValues();
    }
}