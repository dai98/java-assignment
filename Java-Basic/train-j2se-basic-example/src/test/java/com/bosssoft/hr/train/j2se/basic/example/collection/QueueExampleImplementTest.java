package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class QueueExampleImplementTest {

    @Test
    public void add() {
        QueueExampleImplement queueExampleImplement = new QueueExampleImplement();
        queueExampleImplement.add(new User(1,"A"));
        queueExampleImplement.add(new User(2,"B"));
        queueExampleImplement.add(new User(3,"C"));
        assertEquals(3,queueExampleImplement.size());
    }

    @Test
    public void remove() {
        QueueExampleImplement queueExampleImplement = new QueueExampleImplement();
        queueExampleImplement.add(new User(1,"A"));
        queueExampleImplement.add(new User(2,"B"));
        queueExampleImplement.remove();
        assertEquals(1,queueExampleImplement.size());
    }

    @Test
    public void offer() {
        QueueExampleImplement queueExampleImplement = new QueueExampleImplement();
        queueExampleImplement.add(new User(1,"A"));
        queueExampleImplement.add(new User(2,"B"));
        assertTrue(queueExampleImplement.offer(new User(3,"C")));
    }

    @Test
    public void poll() {
        QueueExampleImplement queueExampleImplement = new QueueExampleImplement();
        queueExampleImplement.add(new User(1,"A"));
        queueExampleImplement.add(new User(2,"B"));
        assertEquals(new User(1,"A"),queueExampleImplement.poll());
    }

    @Test
    public void peek() {
        QueueExampleImplement queueExampleImplement = new QueueExampleImplement();
        queueExampleImplement.add(new User(1,"A"));
        queueExampleImplement.add(new User(2,"B"));
        assertEquals(new User(1,"A"),queueExampleImplement.peek());
    }
}