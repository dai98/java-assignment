package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class TreeMapExampleImplementTest {

    @Test
    public void put() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        System.out.println(treeMapExampleImplement.toString());
        assertEquals(1,treeMapExampleImplement.size());
    }

    @Test
    public void remove() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertEquals(1,treeMapExampleImplement.size());
        treeMapExampleImplement.remove(new Role(1,"A"));
        assertEquals(0,treeMapExampleImplement.size());
    }

    @Test
    public void containsKey() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        assertTrue(treeMapExampleImplement.containsKey(new Role(1,"A")));
    }

    @Test
    public void visitByEntryset() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        treeMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        treeMapExampleImplement.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        treeMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        treeMapExampleImplement.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        TreeMapExampleImplement treeMapExampleImplement = new TreeMapExampleImplement();
        treeMapExampleImplement.put(new Role(1,"A"),new Resource(2,"B"));
        treeMapExampleImplement.put(new Role(3,"C"),new Resource(4,"D"));
        treeMapExampleImplement.visitByValues();
    }
}