package com.bosssoft.hr.train.j2se.basic.example.thread;

import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ThreadPoolTest {

    @Test
    public void TestExecute() {
        ThreadPool threadPool = new ThreadPool("Thread Pool",3,5,
                new ArrayBlockingQueue(1024), new DiscardRejectPolicy());
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                for(int i=1;i<=50;i++){
                    System.out.println(i);
                }
            }
        });

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                for(int i=51;i<=100;i++){
                    System.out.println(i);
                }
            }
        });
    }
}