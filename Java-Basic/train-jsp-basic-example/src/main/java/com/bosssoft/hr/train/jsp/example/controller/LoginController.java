package com.bosssoft.hr.train.jsp.example.controller;

import com.alibaba.fastjson.JSON;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @author 戴若汐
 */
@Slf4j
@WebServlet("/login")
public class LoginController extends HttpServlet {

    private static final UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
        String code = "";
        String password ="";

        String codeAttribute = "code";
        // 校验参数合法性 如果没问题才调用
        if (req.getParameter(codeAttribute) != null) {
            code = req.getParameter("code");
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/codeError.jsp").forward(req,resp);
        }

        String passwordAttribute = "password";
        // 校验参数合法性 如果没问题才调用
        if (req.getParameter(passwordAttribute) != null) {
            password = req.getParameter("password");
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/passwordError.jsp").forward(req,resp);
        }

        User user = new User(code,password);
        boolean authenticationResult = userService.authentication(user);
        HashMap<String,Integer> resultMap = new HashMap<>();
        resultMap.put("status",authenticationResult?1:0);

        try{
            PrintWriter printWriter = resp.getWriter();
            printWriter.write(JSON.toJSONString(resultMap));
            printWriter.flush();
            printWriter.close();
        }catch (IOException e){
            log.warn("IO Exception");
        }
    }

}
