package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.pojo.query.QueryAll;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 戴若汐
 */
@Slf4j
@WebServlet("query")
public class QueryUserController extends HttpServlet {

    private static final UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req,resp);
        int id = -1;
        String name = "";
        String code = "";
        String password = "";
        List<User> users = userService.queryByCondition(new QueryAll());
        List<User> resUsers = new ArrayList<>();

        // 校验参数合法性 如果没问题才调用
        String idAttribute = "id";
        if (req.getParameter(idAttribute) != null) {
            try{
                id = Integer.valueOf(req.getParameter("id"));
            }catch(Exception ex){
                log.warn("Wrong Format");
            }
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/idError.jsp").forward(req,resp);
        }

        String nameAttribute = "name";
        if (req.getParameter(nameAttribute) != null) {
            name = req.getParameter("name");
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/nameError.jsp").forward(req,resp);
        }

        String codeAttribute = "code";
        if (req.getParameter(codeAttribute) != null) {
            code = req.getParameter("code");
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/codeError.jsp").forward(req,resp);
        }

        String passwordAttribute = "password";
        if (req.getParameter(passwordAttribute) != null) {
            password = req.getParameter("password");
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/passwordError.jsp").forward(req,resp);
        }

        for(User user:users){
            if(!(id!= -1 && id == user.getId())){
                continue;
            }

            if(!(name!= null && name.equals(user.getName()))){
                continue;
            }

            if(!(code != null && name.equals(user.getCode()))){
                continue;
            }

            if(!(password != null && password.equals(user.getPassword()))){
                continue;
            }
            resUsers.add(user);
        }

        try{
            req.setAttribute("users",resUsers);
            req.getRequestDispatcher("result.jsp").forward(req,resp);
        }catch (BusinessException e){
            log.warn("SQL Exception");
        }
    }
}
