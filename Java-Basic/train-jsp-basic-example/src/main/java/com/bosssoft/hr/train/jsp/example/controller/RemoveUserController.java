package com.bosssoft.hr.train.jsp.example.controller;

import com.alibaba.fastjson.JSON;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
@Slf4j
public class RemoveUserController extends HttpServlet {

    private static final UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        super.doGet(req,resp);
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        super.doPost(req,resp);
        int id = -1;

        // 校验参数合法性 如果没问题才调用
        String idAttribute = "id";
        if (req.getParameter(idAttribute) != null) {
            try{
                id = Integer.valueOf(req.getParameter("id"));
            }catch(Exception ex){
                log.warn("Wrong Format");
            }
        }else{
            // 错误应答 或者跳转错误页面
            req.getRequestDispatcher("src/main/webapp/errorPages/idError.jsp").forward(req,resp);
        }

        User user = new User(id,null);
        HashMap<String,Integer> resultMap = new HashMap<>(1);
        resultMap.put("status",userService.remove(user)?1:0);
        try{
            PrintWriter printWriter = resp.getWriter();
            printWriter.write(JSON.toJSONString(resultMap));
            printWriter.flush();
            printWriter.close();
        }catch (IOException e){
            log.warn("IO Exception");
        }

    }
}
