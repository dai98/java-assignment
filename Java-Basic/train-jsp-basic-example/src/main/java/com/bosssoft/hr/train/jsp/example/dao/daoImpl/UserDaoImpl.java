package com.bosssoft.hr.train.jsp.example.dao.daoImpl;


import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.pojo.query.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 * @since
 **/
public class UserDaoImpl implements UserDao {
    @Override
    public int insert(User user) {
        String id = user.getId() + "";
        String name = user.getName();
        String code = user.getCode();
        String password = user.getPassword();
        StringBuilder sb = new StringBuilder("Insert into t_user (id,name,code,password) values (");
        sb.append(id).append(",")
                .append(name).append(",")
                .append(code).append(",")
                .append(password).append(")");
        String sql = sb.toString();
        return DBUtil.executeUpdate(sql);
    }

    @Override
    public int deleteById(Integer id) {
        StringBuilder sb = new StringBuilder("Delete from t_user where id=");
        String sql = sb.append(id).toString();
        return DBUtil.executeUpdate(sql);
    }

    @Override
    public int update(User user) {
        String id = user.getId() + "";
        String name = user.getName();
        String code = user.getCode();
        String password = user.getPassword();
        StringBuilder sb = new StringBuilder("Update t_user set ");
        sb.append("id=").append(id).append(",")
                .append("name").append(name).append(",")
                .append("code").append(code).append(",")
                .append("password").append(password).append(",")
                .append(" Where id=").append(id);
        String sql = sb.toString();
        return DBUtil.executeUpdate(sql);
    }

    @Override
    public List<User> queryByCondition(Query query) throws SQLException {

        ResultSet rs = query.query();
        List<User> result = new ArrayList<>();
        while(rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String code = rs.getString("code");
            String passwd = rs.getString("password");
            User user = new User(id,name,code,passwd);
            result.add(user);
        }
        return result;
    }
}
