package com.bosssoft.hr.train.jsp.example.listener;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author 戴若汐
 */
@WebListener
public class NumListener implements HttpSessionListener {

    static final String COUNT_ATTRIB = "OnlineCount";

    @Override
    public void sessionCreated(HttpSessionEvent sessionEvent) {
        ServletContext servletContext = sessionEvent.getSession().getServletContext();
        Integer onlineCount = (Integer)servletContext.getAttribute(COUNT_ATTRIB);
        onlineCount = onlineCount == null ? 1 : onlineCount + 1;
        servletContext.setAttribute(COUNT_ATTRIB, onlineCount);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent sessionEvent) {
        ServletContext servletContext = sessionEvent.getSession().getServletContext();
        Integer onlineCount = (Integer)servletContext.getAttribute(COUNT_ATTRIB);
        servletContext.setAttribute(COUNT_ATTRIB, --onlineCount);
    }
}
