package com.bosssoft.hr.train.jsp.example.pojo.query;

import java.sql.ResultSet;

public interface Query{
    public abstract ResultSet query();
}
