package com.bosssoft.hr.train.jsp.example.pojo.query;


import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryAll implements Query{

    private static final String SQL = "select * from t_user";

    @Override
    public ResultSet query() {
        return DBUtil.query(SQL);
    }
}
