package com.bosssoft.hr.train.jsp.example.pojo.query;


import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryByCode implements Query{

    private String code;
    public QueryByCode(String code) {
        this.code = code;
    }

    @Override
    public ResultSet query() {
        String sql = "select * from t_user where code="+this.code;
        return DBUtil.query(sql);
    }
}
