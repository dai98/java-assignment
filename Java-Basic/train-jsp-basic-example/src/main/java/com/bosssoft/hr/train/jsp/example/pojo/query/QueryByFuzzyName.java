package com.bosssoft.hr.train.jsp.example.pojo.query;


import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryByFuzzyName implements Query{

    private String fuzzyName;

    public QueryByFuzzyName(String fuzzyName) {
        this.fuzzyName = fuzzyName;
    }

    @Override
    public ResultSet query() {
        String sql = "select * from t_user where name like " + this.fuzzyName;
        return DBUtil.query(sql);
    }
}