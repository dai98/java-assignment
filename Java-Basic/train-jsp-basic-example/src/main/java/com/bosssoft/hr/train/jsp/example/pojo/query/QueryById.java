package com.bosssoft.hr.train.jsp.example.pojo.query;


import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryById implements Query {

    private int id;
    public QueryById(int id) {
        this.id = id;
    }

    @Override
    public ResultSet query() {
        String sql = "select * from t_user where id=" + this.id;
        return DBUtil.query(sql);
    }
}
