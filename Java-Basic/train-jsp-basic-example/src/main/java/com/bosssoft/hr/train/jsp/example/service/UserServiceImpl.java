package com.bosssoft.hr.train.jsp.example.service;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.exception.BusinessExceptionCode;
import com.bosssoft.hr.train.jsp.example.pojo.query.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.pojo.query.QueryById;

import java.sql.SQLException;
import java.util.List;

/**
 * @author 戴若汐
 */
public class UserServiceImpl implements  UserService{

    UserDao dao = new UserDaoImpl();

    @Override
    public boolean save(User user){
        try {
            return dao.insert(user) > 0;
        }catch (Exception ex){
            throw new BusinessException(BusinessExceptionCode.CODE_1,ex.getMessage(),ex);
        }

    }

    @Override
    public boolean remove(User user) {
        return dao.deleteById(user.getId()) > 0;
    }

    @Override
    public boolean update(User user) {
        return dao.update(user) > 0;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition){
        try{
            return dao.queryByCondition(queryCondition);
        }catch(SQLException e) {
            throw new BusinessException(BusinessExceptionCode.CODE_1, e.getMessage(), e);
        }
    }

    @Override
    public boolean authentication(User user){
        try{
            int id = user.getId();
            User userRes = dao.queryByCondition(new QueryById(id)).get(0);
            return userRes.getPassword().equals(user.getPassword());
        }catch (SQLException e){
            throw new BusinessException(BusinessExceptionCode.CODE_1,e.getMessage(),e);
        }
    }
}
