package com.bosssoft.hr.train.jsp.example.tag;

import com.bosssoft.hr.train.jsp.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @description: 定义<boss:userTag /> 标签
 * @author: Administrator
 * @create: 2020-05-29 13:50
 * @since
 **/
@Slf4j
public class UserTag extends TagSupport {

    private User user;
    @Override
    public int doStartTag(){
        try{
            JspWriter out = this.pageContext.getOut();
            if (user == null){
                out.println("No User Find");
                return SKIP_BODY;
            }
            out.print(user.getId() + " ");
            out.print(user.getName() + " ");
            out.print(user.getCode() + " ");
            out.print(user.getPassword() + " ");
            out.println();
        }catch (IOException e){
            log.info(e.toString());
        }
        return SKIP_BODY;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
