<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %>

<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xml:lang="aa">
<head>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <meta charset="UTF-8">
    <title>新增用户</title>
    <script>

        $(function () {
            $("#insert").click(function () {
                var username = $("#username").val();//取值
                var password = $("#password").val();
                var age = $("#age").val();
                if (!username) {
                    alert("用户名必填!");
                    $("#username").focus();//获取焦点
                    return;
                }
                if (!password) {
                    alert("密码必填!");
                    $("#password").focus();//获取焦点
                    return;
                }
                if (!age) {
                    alert("年龄必填!");
                    $("#age").focus();//获取焦点
                    return;
                }
                window.location.href = "insert?name=" + username + "&pwd=" + password + "&age=" + age;
            });
        });
        // }
    </script>
</head>
<style type="text/css">
    .input1 {
        border: 1px solid #CDC28D;
        background-color: #F2F9FD;
        height: 40px;
        width: 200px;
        padding-top: 4px;
        font-size: 12px;
        padding-left: 10px;
    }

    .input2 {
        border: 1px solid #1E1E1E;
        background-color: #F2F9FD;
        height: 30px;
        width: 80px;
    }
</style>

<body>
<table>
    <caption>输入用户信息</caption>
    <tr>
        <th scope="row">ID:</th>
        <th scope="row">姓名:</th>
        <th scope="row">代码:</th>
        <th scope="row">密码:</th>
    </tr>
    <tr height="150dp">
        <td></td>
    </tr>
    <tr>
        <td colspan="2"><h1>新增用户</h1></td>
    </tr>
    <tr>
        <td><input class="input1" type="text" name="id" id="id"/></td>
    </tr>
    <tr>
        <td><input class="input1" type="text" name="name" id="username"/></td>
    </tr>
    <tr>
        <td><input class="input1" type="text" name="code" id="code"/></td>
    </tr>
    <tr>
        <td><input class="input1" type="password" name="password" id="password"/></td>
    </tr>
    <tr>
        <td colspan="2"><input class="input2" type="submit" value="新增" id="insert"/></td>
    </tr>
</table>
</body>
</html>
