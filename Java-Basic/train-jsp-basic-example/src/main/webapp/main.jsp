<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.bosssoft.hr.train.jsp.example.util.DBUtil" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xml:lang="aa">
<head>
    <meta charset="UTF-8">
    <title>操作用户</title>

</head>
<body>

<%
    //    int index = (int) session.getAttribute("current");
    List<User> users = (List) session.getAttribute("userList");
%>

<table>
    <caption>用户信息</caption>
    <tr>
        <th scope="row">
            编号
        </th>
        <th scope="row">
            姓名
        </th>
        <th scope="row">
            代码
        </th>
        <th scope="row">
            密码
        </th>
        <th scope="row">
            操作
        </th>
    </tr>
    <tbody>
    <tr>

        <%
            for (User user : users) {
        %>
        <td><%=user.getId()%>
        </td>
        <td><%=user.getName()%>
        </td>
        <td><%=user.getCode()%>
        </td>
        <td><%=user.getPassword()%>
        </td>
        <td>
            <a href="delete?id=<%=user.getId() %>">删除</a>
            <a href="update?id=<%=user.getId() %>">更新</a>
        </td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
    <a href="insert.jsp">新增</a>
    <a href="search.jsp">查询</a>
</body>
</html>
