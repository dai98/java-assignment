package com.boss.dai.mybatis.mapper;

import com.boss.dai.mybatis.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * xml映射
 * 在这里使用@Mapper注解或在主类中使用@MapperScan注解将该接口扫描装配到容器中
 * 方式一  @MapperScan
 * 方式二  @Mapper +  application.yml mapper-locations: classpath*:com/boss/db/mapper/*.xml
 *
 * 二级缓存设置方式：
 * 方式一：@CacheNamespace
 * 方式二：UserXmlMapper.xml <cache></cache>
 */
@Mapper
public interface UserXmlMapper {
    //新增用户
    int insertUser(User user);

    // 修改用户
    void updateUser(User user);

    //查询所有的用户
    List<User> selectUsersByCondition(User user);
    
    //查询所有的用户
    List<User> selectUsersByCondition2(User user);

}