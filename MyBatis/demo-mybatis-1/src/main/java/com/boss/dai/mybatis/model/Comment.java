package com.boss.dai.mybatis.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: 评论
 * @author: renyr
 * @create: 2019/12/04 16:29
 */
public class Comment implements Serializable {
    private Long id;
    private Long blogId;
    private String content;
    private Date commentDate;

    private Blog blog;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }
}
