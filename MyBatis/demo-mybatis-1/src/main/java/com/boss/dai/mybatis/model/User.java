package com.boss.dai.mybatis.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: 用户信息
 * @author: renyr
 * @create: 2019/11/24 20:41
 */
public class User implements Serializable{
    private Long userId;
    private String userName;
    private String userPassword;
    private Date userBirth;
    private String userAddress;
    private String userPhone;
    private int userStatus;

    public User(){
    }

    public  User(String userName,String userPassword,Date userBirth,String userAddress,String userPhone,int userStatus){
        this.userName = userName;
        this.userPassword = userPassword;
        this.userBirth = userBirth;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.userStatus = userStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Date getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(Date userBirth) {
        this.userBirth = userBirth;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public String toString(){
        return "User[userId="+userId+",userName="+userName+",userPassword="+userPassword+",userBirth="+userBirth+"," +
                "userPhone="+userPhone+",userAddress="+userAddress+",userStatus="+userStatus+"]";
    }
}
