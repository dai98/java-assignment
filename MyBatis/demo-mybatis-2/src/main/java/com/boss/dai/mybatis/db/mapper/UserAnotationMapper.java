package com.boss.dai.mybatis.db.mapper;


import com.boss.dai.mybatis.db.model.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.sql.Date;
import java.util.List;

/**
 * @description: 注解
 * CacheNamespace  开启了二级缓存
 * @author: renyr
 * @create: 2019/12/02 10:18
 */
@Mapper
@CacheNamespace
public interface UserAnotationMapper {
    /**
     *  新增用户
     */
    @Insert({
        "insert into USER_INFO (USER_NAME,USER_PASSWORD,USER_BIRTH,USER_ADDRESS,USER_PHONE,USER_STATUS) ",
        "values(#{userName},#{userPassword},#{userBirth},#{userAddress},#{userPhone},#{userStatus})"
    })
    @Options(useGeneratedKeys = true, keyColumn = "USER_ID", keyProperty = "userId")
    int insertUser(User user);

    /**
     *  修改用户
     */
    @Update({
        "<script>",
        " update USER_INFO ",
        "<set>",
        "<if test=\"userName!=null\">USER_NAME=#{userName},</if>",
        "<if test=\"userPassword!=null\">USER_PASSWORD=#{userPassword},</if>",
        "<if test=\"userBirth!=null\">USER_BIRTH=#{userBirth},</if>",
        "<if test=\"userAddress!=null\">USER_ADDRESS=#{userAddress},</if>",
        "<if test=\"userPhone!=null\">USER_PHONE=#{userPhone},</if>",
        "<if test=\"userStatus!=null\">USER_STATUS=#{userStatus}</if>",
        "</set>",
        "where USER_ID = #{userId}",
        "</script>"
    })
    void updateUser(User user);


    /**
     *  查询用户
     */
    @Select({
        "<script>",
        "select USER_ID,USER_NAME,USER_PASSWORD,USER_BIRTH,USER_ADDRESS,USER_PHONE,USER_STATUS from USER_INFO",
        "<where>",
        "    <if test=\"userName != null\">",
        "        AND USER_NAME LIKE CONCAT('%',#{userName},'%')",
        "    </if>",
        "    <if test=\"userPassword != null\">",
        "        AND USER_PASSWORD=#{userPassword}",
        "    </if>",
        "    <if test=\"userAddress!=null\">",
        "        AND USER_ADDRESS=#{userAddress}",
        "    </if>",
        "</where>",
        "</script>"
    })
    @Results(
            id = "user",value = {
            @Result(column = "USER_ID", property = "userId", javaType = Long.class, jdbcType = JdbcType.BIGINT),
            @Result(column = "USER_NAME", property = "userName", javaType = String.class, jdbcType = JdbcType.VARCHAR),
            @Result(column = "USER_PASSWORD", property = "userPassword", javaType = String.class, jdbcType = JdbcType.VARCHAR),
            @Result(column = "USER_BIRTH", property = "userBirth", javaType = Date.class, jdbcType = JdbcType.DATE),
            @Result(column = "USER_ADDRESS", property = "userAddress", javaType = String.class, jdbcType = JdbcType.VARCHAR),
            @Result(column = "USER_PHONE", property = "userPhone", javaType = String.class, jdbcType = JdbcType.VARCHAR),
            @Result(column = "USER_STATUS", property = "userStatus", javaType = Integer.class, jdbcType = JdbcType.INTEGER)
    })
    List<User> selectUsersByCondition(User user);
}
