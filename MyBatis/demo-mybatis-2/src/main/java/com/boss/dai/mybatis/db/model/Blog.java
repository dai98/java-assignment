package com.boss.dai.mybatis.db.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @description: 博客
 * @author: renyr
 * @create: 2019/12/04 16:29
 */
public class Blog implements Serializable {

    private Long id;
    private Long userId;
    private String title;
    private String content;
    private int type;
    private Date createTime;

    private List<Comment> comments;

    private Long[] ids;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    @Override
    public String toString(){
        return "Blog[id="+id+",title="+title+",content="+content+",type="+type+"]";
    }
}
