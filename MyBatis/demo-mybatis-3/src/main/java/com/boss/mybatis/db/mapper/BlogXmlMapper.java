package com.boss.mybatis.db.mapper;

import com.boss.mybatis.db.model.Blog;
import com.boss.mybatis.db.model.Comment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * xml映射
 * 在这里使用@Mapper注解或在主类中使用@MapperScan注解将该接口扫描装配到容器中
 * 方式一  @MapperScan
 * 方式二  @Mapper +  application.yml mapper-locations: classpath*:com/boss/db/mapper/*.xml
 *
 * 二级缓存设置方式：
 * 方式一：@CacheNamespace
 * 方式二：UserXmlMapper.xml <cache></cache>
 */
@Mapper
public interface BlogXmlMapper {

    List<Blog> selectBlogList();

    List<Comment> selectCommentList();

    List<Blog> qryBlogList();

    List<Blog> qryBlogListByCondition(Blog blog);

    List<Blog> qryBlogListByCondition2(Blog blog);

}