package com.boss.mybatis.db.controller;

import com.boss.mybatis.db.bean.TbUser;
import com.boss.mybatis.db.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/user")
public class TbUserController {
    @Resource
    UserService userService;

    @RequestMapping("/findAll")
    public List<TbUser> findAll() {
        return userService.findAll();
    }

    @RequestMapping("/findById/{id}")
    public TbUser findById(@PathVariable int id) {
        return userService.findById(id);
    }

    @RequestMapping("/findByUsername/{username}")
    public List<TbUser> findByUsername(@PathVariable String username) {
        return userService.findByUsername(username);
    }

    @PostMapping("/find")
    public List<TbUser> find(TbUser user) {
        return userService.find(user);
    }

    @PostMapping("/add")
    public boolean addUser(TbUser user) {
        return userService.addUser(user);
    }

    @PostMapping("/update")
    public boolean updateUser(TbUser user) {
        return userService.updateUser(user);
    }

    @PostMapping("/remove")
    public boolean removeUser(TbUser user) {
        return userService.removeUser(user);
    }
}
