package com.boss.mybatis.db.dao;

import com.boss.mybatis.db.bean.TbUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TbUserDao {
    List<TbUser> findAll();

    TbUser findById(int id);

    List<TbUser> findByUsername(String username);

    List<TbUser> find(TbUser user);

    boolean addUser(TbUser user);

    boolean updateUser(TbUser user);

    boolean removeUser(TbUser user);
}
