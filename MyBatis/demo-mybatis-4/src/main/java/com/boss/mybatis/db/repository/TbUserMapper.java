package com.boss.mybatis.db.repository;

import java.util.List;

import com.boss.mybatis.db.bean.TbUser;
import com.boss.mybatis.db.bean.TbUserExample;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface TbUserMapper {
    long countByExample(TbUserExample example);

    int deleteByExample(TbUserExample example);

    @Delete({
            "delete from tb_user",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
            "insert into tb_user (id, login_name, ",
            "username, password, ",
            "db_source)",
            "values (#{id,jdbcType=INTEGER}, #{loginName,jdbcType=VARCHAR}, ",
            "#{username,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
            "#{dbSource,jdbcType=VARCHAR})"
    })
    int insert(TbUser record);

    int insertSelective(TbUser record);

    List<TbUser> selectByExample(TbUserExample example);

    @Select({
            "select",
            "id, login_name, username, password, db_source",
            "from tb_user",
            "where id = #{id,jdbcType=INTEGER}"
    })
    @ResultMap("edu.neu.springcloud.repository.TbUserMapper.BaseResultMap")
    TbUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TbUser record, @Param("example") TbUserExample example);

    int updateByExample(@Param("record") TbUser record, @Param("example") TbUserExample example);

    int updateByPrimaryKeySelective(TbUser record);

    @Update({
            "update tb_user",
            "set login_name = #{loginName,jdbcType=VARCHAR},",
            "username = #{username,jdbcType=VARCHAR},",
            "password = #{password,jdbcType=VARCHAR},",
            "db_source = #{dbSource,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(TbUser record);
}