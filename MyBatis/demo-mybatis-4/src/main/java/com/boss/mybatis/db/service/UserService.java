package com.boss.mybatis.db.service;


import com.boss.mybatis.db.bean.TbUser;

import java.util.List;

public interface UserService {
    List<TbUser> findAll();

    TbUser findById(int id);

    List<TbUser> findByUsername(String username);

    List<TbUser> find(TbUser user);

    boolean addUser(TbUser user);

    boolean updateUser(TbUser user);

    boolean removeUser(TbUser user);
}
