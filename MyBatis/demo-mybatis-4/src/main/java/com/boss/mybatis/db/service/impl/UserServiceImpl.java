package com.boss.mybatis.db.service.impl;

import com.boss.mybatis.db.bean.TbUser;
import com.boss.mybatis.db.bean.TbUserExample;
import com.boss.mybatis.db.dao.TbUserDao;

import com.boss.mybatis.db.repository.TbUserMapper;
import com.boss.mybatis.db.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Resource
    TbUserMapper tbUserMapper;
    @Autowired
    TbUserDao tbUserDao;

    @Override
    public List<TbUser> findAll() {
        TbUserExample example = new TbUserExample();
        return tbUserMapper.selectByExample(example);
    }

    @Override
    public TbUser findById(int id) {
        return tbUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TbUser> findByUsername(String username) {
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        return tbUserMapper.selectByExample(tbUserExample);
    }

    @Override
    public List<TbUser> find(TbUser user) {
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        if (user.getId() != null) {
            criteria.andIdEqualTo(user.getId());
        }

        if (user.getLoginName() != null) {
            criteria.andLoginNameEqualTo(user.getLoginName());
        }

        if (user.getPassword() != null) {
            criteria.andPasswordEqualTo(user.getPassword());
        }

        if (user.getUsername() != null) {
            criteria.andUsernameEqualTo(user.getUsername());
        }

        if (user.getDbSource() != null) {
            criteria.andDbSourceEqualTo(user.getDbSource());
        }

        return tbUserMapper.selectByExample(tbUserExample);
    }

    @Override
    public boolean addUser(TbUser user) {
        return tbUserDao.addUser(user);
    }

    @Override
    public boolean updateUser(TbUser user) {
        return tbUserDao.updateUser(user);
    }

    @Override
    public boolean removeUser(TbUser user) {
        return tbUserDao.removeUser(user);
    }
}
