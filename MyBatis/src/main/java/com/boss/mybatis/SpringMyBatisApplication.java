package com.boss.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @author: renyr
 * @create: 2019/12/02 09:56
 */
@SpringBootApplication
//@MapperScan("com.boss.db.mapper")
public class SpringMyBatisApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringMyBatisApplication.class, args);
    }
}
