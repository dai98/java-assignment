package com.boss.mybatis.db.model;


import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description: TODO
 * @author: renyr
 * @create: 2019/12/04 15:31
 */
public class ExampleTypeHandler implements TypeHandler {
    @Override
    public void setParameter(PreparedStatement ps, int i, Object 		parameter, JdbcType jdbcType) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ps.setString(i, sdf.format(parameter));
    }
    @Override
    public Date getResult(ResultSet rs, String columnName)
            throws SQLException {
        return new Date(Long.valueOf(rs.getString(columnName)));
    }

    @Override
    public Object getResult(ResultSet resultSet, int i) throws SQLException {
        return new Date(resultSet.getLong(i));
    }

    @Override
    public Date getResult(CallableStatement cs, int columnIndex)
            throws SQLException {
        return new Date(Long.valueOf(cs.getString(columnIndex)));
    }
}

