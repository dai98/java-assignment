package com.boss.mybatis.db.mapper;

import com.boss.mybatis.db.model.Blog;
import com.boss.mybatis.db.model.Comment;
import com.boss.mybatis.db.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: xml映射测试
 * @author: renyr
 * @create: 2019/12/02 10:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//@ActiveProfiles("xml")
public class BlogXmlMapperTest {

    @Autowired
    BlogXmlMapper blogXmlMapper;
    /**
     * 查询所有博客
     */
    @Test
    public void selectBlogListTest() throws Exception{
        List<Blog> blogList = blogXmlMapper.selectBlogList();
        for(Blog blog: blogList){
            System.out.println("======================博客标题："+blog.getTitle()+"=======================");
            /*List<Comment> commentList = blog.getComments();
            for(Comment comment:commentList){
                System.out.println("评论："+comment.getContent());
            }*/
        }
    }


    /**
     * 查询所有评论
     */
    @Test
    public void selectCommentListTest() throws Exception{
        List<Comment> commentList = blogXmlMapper.selectCommentList();
        for(Comment comment:commentList){
            System.out.println("==========================评论："+comment.getContent()+"==============================");
            Blog blog = comment.getBlog();
            System.out.println("博客信息[title="+blog.getTitle()+",content="+blog.getContent()+"]");
        }
    }

    /**
     * 嵌套结果查询所有博客
     */
    @Test
    public void qryBlogListTest () throws Exception{
        List<Blog> blogList = blogXmlMapper.qryBlogList();
        for(Blog blog: blogList){
            System.out.println("======================博客标题："+blog.getTitle()+"=======================");
            List<Comment> commentList = blog.getComments();
            for(Comment comment:commentList){
                System.out.println("评论："+comment.getContent());
            }
        }
    }

    /**
     * 动态sql
     */
    @Test
    public void qryBlogListByConditionTest () throws Exception{
        Blog condition = new Blog();

        /*condition.setTitle("一");
        condition.setContent("二");*/

        Long[] ids = new Long[]{1L,2L};
        condition.setIds(ids);

        List<Blog> blogList = blogXmlMapper.qryBlogListByCondition(condition);
        for(Blog blog: blogList){
            System.out.println(blog);
        }
    }
}
