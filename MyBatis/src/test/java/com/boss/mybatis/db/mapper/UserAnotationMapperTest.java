package com.boss.mybatis.db.mapper;

import com.boss.mybatis.db.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @description: TODO
 * @author: renyr
 * @create: 2019/11/24 20:16
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("anno")
public class UserAnotationMapperTest {

    @Autowired
    UserAnotationMapper userMapper;




    /**
     * 查询测试
     */
    @Test
    public void testSelectUsers() throws Exception{
        User condition = new User();
        //condition.setUserName("露西");
        //查询所有用户信息
        List<User> users = userMapper.selectUsersByCondition(condition);
        //输出结果
        System.out.println("所有用户的名字是：");
        for (User user : users) {
            System.out.println(user);
        }
    }

    /**
     * 查询二级缓存测试
     *
     */
    @Test
    public void qryCacheTest() throws Exception{
        User condition = new User();
        //condition.setUserName("露西");
        //查询所有用户信息
        List<User> users = userMapper.selectUsersByCondition(condition);
        //输出结果
        System.out.println("开始第一次查询：");
        for (User user : users) {
            System.out.println(user);
        }

        //查询所有用户信息
        List<User> users2 = userMapper.selectUsersByCondition(condition);
        //输出结果
        System.out.println("开始第二次查询：");
        for (User user : users2) {
            System.out.println(user);
        }
    }
}
