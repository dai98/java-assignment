package com.boss.dai.redis.dao;

import com.boss.dai.redis.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 戴若汐
 */
@Repository
@Mapper
public interface UserDbDao {

    /**
     * 返回所有用户的列表
     * @return 用户返回值
     */
    List<User> findAll();
}
