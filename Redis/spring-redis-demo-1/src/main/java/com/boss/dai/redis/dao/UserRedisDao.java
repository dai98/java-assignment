package com.boss.dai.redis.dao;

import com.boss.dai.redis.bean.User;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 戴若汐
 */
@Repository
public class UserRedisDao {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private UserDbDao userDbDao;


    public Object insert() {
        int cacheTime = 30;
        String cacheKey = "produce_list";
        String cacheSign = cacheKey + "_sign";
        String sign = redisTemplate.opsForValue().get(cacheSign);
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        if (sign != null){
            return cacheValue;
        }else{
            redisTemplate.opsForValue().set(cacheSign,"1",cacheTime);
            List<User> cacheDbValue = userDbDao.findAll();
            for(User user:cacheDbValue){
                redisTemplate.opsForValue().set(cacheKey,user.toString(),cacheTime*2);
            }
            return cacheDbValue;
        }
    }

    public String query(String key){
        String result = redisTemplate.opsForValue().get(key);
        if(null == result){
            redisTemplate.opsForValue().set(key, null, 60 * 5, TimeUnit.SECONDS);
        }
        return result;
    }
}
