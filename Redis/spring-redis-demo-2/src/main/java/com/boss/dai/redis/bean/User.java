package com.boss.dai.redis.bean;

import lombok.*;

/**
 * @author 戴若汐
 */

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int id;
    private String code;
    private String name;
}
