package com.boss.dai.redis.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.time.Duration;

/**
 * @author 戴若汐
 */
@Repository
public class UserRedisDao {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private UserDbDao userDbDao;

    public String get(String key) throws InterruptedException {
        String value = redisTemplate.opsForValue().get(key);
        String tempValue = "1";
        long holdTime = 180L;
        if(value == null){
            Boolean result = redisTemplate.opsForValue().setIfAbsent(key, tempValue, Duration.ofSeconds(holdTime));
            if (result!=null && result){
                String dbValue = userDbDao.findById(key).toString();
                redisTemplate.opsForValue().set(key,dbValue);
                redisTemplate.delete(key);
            }else{
                Thread.sleep(50);
                return redisTemplate.opsForValue().get(key);
            }
        }else{
            return value;
        }
        return null;
    }
}
