package com.boss.dai.spring;


import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * @author 戴若汐
 * 使用Junit测试 不定义main方法了
 */
@Slf4j
public class App {

    public static void main(String[] args) {
        beanFactoryDemo();
        applicationContextDemo();
    }

    /**
     * 回报BeanFactory已经过时了
     * 仅实验目的 开发请用ApplicationContext
     */
    public static void beanFactoryDemo(){
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        UserService userService = beanFactory.getBean(UserService.class);
        User user = new User("BeanFactory 测试用户");
        userService.saveUser(user);
        userService.updateUser(user);
        User user2 = userService.getUser();
        log.info("获取用户 "+user2.getName());
    }

    public static void applicationContextDemo(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = applicationContext.getBean(UserService.class);
        User user = new User("ApplicationContext 测试用户");
        userService.saveUser(user);
        userService.updateUser(user);
        User user2 = userService.getUser();
        log.info("获取用户 "+user2.getName());
    }
}
