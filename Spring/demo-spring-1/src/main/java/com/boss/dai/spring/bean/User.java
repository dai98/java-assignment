package com.boss.dai.spring.bean;

/**
 * @author 戴若汐
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
