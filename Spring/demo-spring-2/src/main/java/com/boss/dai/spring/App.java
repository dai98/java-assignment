package com.boss.dai.spring;


import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.service.impl.UserServicePrototypeImpl;
import com.boss.dai.spring.service.impl.UserServiceSingletonImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author 戴若汐
 */
@Slf4j
public class App {

    public static void main(String[] args){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        singletonUserServiceDemo(context);
        prototypeUserServiceDemo(context);
        ((ConfigurableApplicationContext)context).close();
    }

    /**
     * 查看单例Bean 查看Scope
     */
    public static void singletonUserServiceDemo(ApplicationContext context){
        for(int i=0;i<3;i++){
            UserServiceSingletonImpl userServiceSingleton = context.getBean(UserServiceSingletonImpl.class);
            // 打印地址查看是否为同一个实例
            log.info(userServiceSingleton.toString());
            User user = userServiceSingleton.getUser();
            userServiceSingleton.saveUser(user);
            userServiceSingleton.updateUser(user);
        }
    }
    /**
     *
     */
    public static void prototypeUserServiceDemo(ApplicationContext context){
        for(int i=0;i<3;i++){
            UserServicePrototypeImpl userServicePrototype = context.getBean(UserServicePrototypeImpl.class);
            // 打印地址查看是否为同一个实例
            log.info(userServicePrototype.toString());
            User user = userServicePrototype.getUser();
            userServicePrototype.saveUser(user);
            userServicePrototype.updateUser(user);
        }
    }
}
