package com.boss.dai.spring.service;

import com.boss.dai.spring.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 戴若汐
 */

@Service
@Slf4j
public class UserService {

    public void saveUser(User user){
        log.info("saveUser: "+user.getName());
    }

    public void updateUser(User user){
        log.info("updateUser: "+user.getName());
    }

    public User getUser(){
        return new User("测试用户");
    }

    public void init(){
        log.info("UserService初始化");
    }

    public void destroy(){
        log.info("UserService销毁");
    }

}
