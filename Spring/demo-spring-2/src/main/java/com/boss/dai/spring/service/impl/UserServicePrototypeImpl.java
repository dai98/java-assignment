package com.boss.dai.spring.service.impl;

import com.boss.dai.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 戴若汐
 */

@Service
@Slf4j
public class UserServicePrototypeImpl extends UserService {
}
