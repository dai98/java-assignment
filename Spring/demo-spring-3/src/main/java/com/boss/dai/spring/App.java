package com.boss.dai.spring;

import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 戴若汐
 */
@Slf4j
public class App {
    /**
     * 获取两个相同的Bean 查看他们的地址是不是同一个 来查看是不是被缓存了
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        for(int i=0;i<3;i++){
            UserService userService = context.getBean(UserService.class);
            // 打印地址查看是否为同一个实例
            log.info(userService.toString());
            User user = userService.getUser();
            userService.saveUser(user);
            userService.updateUser(user);
        }
    }
}
