package com.boss.dai.spring.bean;

import org.springframework.cache.annotation.Cacheable;

/**
 * @author 戴若汐
 */
@Cacheable("cache1")
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
