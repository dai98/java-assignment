package com.boss.dai.spring.service;

import com.boss.dai.spring.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author 戴若汐
 */

@Service
@Slf4j
public class UserService {

    @Cacheable(value = "users", key = "#save")
    public void saveUser(User user){
        log.info("saveUser: "+user.getName());
    }

    @Cacheable(value = "users", key = "#update")
    public void updateUser(User user){
        log.info("updateUser: "+user.getName());
    }

    @CachePut("users")
    public User getUser(){
        return new User("测试用户");
    }

    @CacheEvict(value="users", beforeInvocation=true, allEntries = true)
    public void deleteUsers(Integer id) {
        log.info("delete user by id: " + id);
    }
}
