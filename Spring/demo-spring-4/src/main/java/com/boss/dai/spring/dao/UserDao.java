package com.boss.dai.spring.dao;

import com.boss.dai.spring.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.object.BatchSqlUpdate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * @author 戴若汐
 */
@Repository
@Slf4j
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    static class UserRowMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setName(resultSet.getString("name"));
            return user;
        }
    }

    /**
     * 增加用户
     * @return 返回的User数组
     */
    public List<User> findAll(){
        String sql = "SELECT * FROM tb_user";
        return jdbcTemplate.query(sql, new UserRowMapper());
    }

    /**
     * 添加用户
     * @param user User类对象
     * @return 受影响的行数
     */
    public int add(User user){
        String sql = "INSERT tb_user (name) values (?)";
        return jdbcTemplate.update(sql,user.getName());
    }

    /**
     * 通过ID查找用户
     * @param id 用户id
     * @return User对象
     */
    public User findById(int id){
        String sql = "SELECT * FROM tb_user WHERE id = ?";
        User user = null;
        // 如果查询结果为空 会抛出EmptyResultDataAccessException
        try{
            user = jdbcTemplate.queryForObject(sql,new Object[]{id},new UserRowMapper());
        }catch (EmptyResultDataAccessException e){
            log.warn(e.toString());
        }
        return user;
    }

    /**
     * 更新用户
     * @param user User对象
     * @return 受影响的行数
     */
    public int update(User user){
        String sql = "UPDATE FROM tb_user SET name = ? WHERE id=?";
        return jdbcTemplate.update(sql,user.getName(),user.getId());
    }

    /**
     * 删除用户
     * @param id 用户id
     * @return 受影响的行数
     */
    public int delete(Integer id){
        String sql = "DELETE FROM tb_user where id=?";
        return jdbcTemplate.update(sql,id);
    }

    @Transactional(isolation= Isolation.DEFAULT,propagation = Propagation.REQUIRED,rollbackFor = SQLException.class)
    public void deleteBatch(Integer[] ids){
        DataSource dataSource = this.getDataSource();
        BatchSqlUpdate batchSqlUpdate = new BatchSqlUpdate(dataSource, "DELETE FROM tb_user where id=?" );
        batchSqlUpdate.setBatchSize(4);
        batchSqlUpdate.setTypes(new int[]{Types.VARCHAR});
        for(int id:ids){
            log.info(String.valueOf(batchSqlUpdate.update(id,id)));
        }
        batchSqlUpdate.flush();
    }

    private DataSource getDataSource(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        return context.getBean(DataSource.class);
    }
}
