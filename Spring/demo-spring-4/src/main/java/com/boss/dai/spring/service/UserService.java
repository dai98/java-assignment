package com.boss.dai.spring.service;


import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.dao.UserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 戴若汐
 */

@Service
@Slf4j
public class UserService {
    @Autowired
    private UserDao userDao;

    public List<User> findAll(){
        return userDao.findAll();
    }

    public int add(User user){
        return userDao.add(user);
    }

    public User findById(int id){
        return userDao.findById(id);
    }

    public int update(User user){
        return userDao.update(user);
    }

    public int delete(Integer id){
        return userDao.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        userDao.deleteBatch(ids);
    }
}
