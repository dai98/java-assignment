package com.boss.dai.spring.service;

import com.boss.dai.spring.bean.User;
import org.junit.Test;


public class UserServiceTest {

    @Test
    public void findAll() {
        UserService userService = new UserService();
        userService.findAll();
    }

    @Test
    public void add() {
        UserService userService = new UserService();
        User user = new User(20,"测试");
        userService.add(user);
    }

    @Test
    public void findById() {
        UserService userService = new UserService();
        userService.findById(20);
    }

    @Test
    public void update() {
        UserService userService = new UserService();
        User user = new User(20,"测试用户");
        userService.update(user);
    }

    @Test
    public void delete() {
        UserService userService = new UserService();
        userService.delete(20);
    }

    @Test
    public void deleteBatch() {
        UserService userService = new UserService();
        userService.deleteBatch(new Integer[]{1,2,3});
    }
}
