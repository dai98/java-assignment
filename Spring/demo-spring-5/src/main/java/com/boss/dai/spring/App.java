package com.boss.dai.spring;

import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.service.SysService;
import com.boss.dai.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = context.getBean(UserService.class);
        SysService sysService = context.getBean(SysService.class);
        User user = new User("Test");
        userService.saveUser(user);
        userService.updateUser(user);
        User user2 = userService.getUser();
        log.info(user2.toString());
        log.info("User2 " + user2.getName());

        sysService.getTimeStamp();
    }
}
