package com.boss.dai.spring.common;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
@Slf4j
public class LogAspect {

    @Pointcut("execution(* com.boss.dai.spring.service.*.*(..))")
    public void pointcut(){
        /**
         * Pointcut
         */
    }

    @Around("pointcut()")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        long starttime = System.currentTimeMillis();
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getSignature().getDeclaringTypeName();
        Object[] args = joinPoint.getArgs();
        String operation = "";
        String desc = "";
        Object result = null;

        Method[] methods = joinPoint.getTarget().getClass().getMethods();
        for(Method method:methods){
            if(method.getName().equals(methodName)){
                MyLog myLog = method.getAnnotation(MyLog.class);
                operation = myLog.action();
                desc = myLog.desc();
                break;
            }
        }

        try{
            result = joinPoint.proceed();
        }catch (Exception e){
            log.error(e.toString());
        }

        long respTime = System.currentTimeMillis() - starttime;

        Map<String,Object> logMap = new HashMap<>();
        logMap.put("time",time);
        logMap.put("className",className);
        logMap.put("methodName",methodName);
        logMap.put("args",args);
        logMap.put("operation",operation);
        logMap.put("desc",desc);
        logMap.put("result",result);
        logMap.put("respTime",respTime);

        log.info("log "+logMap);

        return result;
    }
}
