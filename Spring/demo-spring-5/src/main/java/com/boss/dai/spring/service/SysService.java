package com.boss.dai.spring.service;

import com.boss.dai.spring.common.MyLog;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SysService {

    @MyLog(action = "获取当前时间戳")
    public long getTimeStamp(){
        long timestamp = System.currentTimeMillis();
        log.info("getTimeStamp: "+timestamp);
        return timestamp;
    }
}
