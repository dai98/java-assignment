package com.boss.dai.spring.service;


import com.boss.dai.spring.bean.User;
import com.boss.dai.spring.common.MyLog;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserService {

    @MyLog(action="添加用户",desc="新增用户描述")
    public void saveUser(User user){
        log.info("saveUser: "+user.getName());
    }

    @MyLog(action="更新用户")
    public void updateUser(User user){
        log.info("updateUser: "+user.getName());
    }

    @MyLog(action = "获取用户")
    public User getUser(){
        return new User("测试");
    }

}
