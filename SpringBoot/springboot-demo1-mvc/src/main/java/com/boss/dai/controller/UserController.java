package com.boss.dai.controller;

import com.boss.dai.bean.User;
import com.boss.dai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 戴若汐
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/listAll")
    public ModelAndView index(){
        ModelAndView mv = new ModelAndView();
        List<User> users = userService.getAll();
        mv.addObject(users);
        return mv;
    }
}
