package com.boss.dai.dao;

import com.boss.dai.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author 戴若汐
 */
@Mapper
@Repository
public interface UserDao {

    /**
     * 获取所有用户
     * @return 用户数组
     */
    List<User> getAll();

    /**
     * 按照id查询用户
     * @param id 用户id
     * @return 用户对象
     */
    User getUserById(int id);

    /**
     * 删除用户
     * @param id 用户id
     */
    void delete(int id);

    /**
     * 添加用户
     * @param user 用户对象
     */
    void add(User user);

    /**
     * 修改用户
     * @param user 用户对象
     */
    void update(User user);
}
