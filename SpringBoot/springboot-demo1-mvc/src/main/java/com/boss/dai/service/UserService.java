package com.boss.dai.service;

import com.boss.dai.bean.User;
import com.boss.dai.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author 戴若汐
 */
@Service
@Validated
public class UserService {
    @Autowired
    UserDao userDao;

    public List<User> getAll(){
        return userDao.getAll();
    }

    public User getUserId(@Size(min = 0) int id){
        return userDao.getUserById(id);
    }

    public void delete(@Size(min = 0) int id){
        userDao.delete(id);
    }

    @NotNull(message = "User对象不能为Null")
    public void add(User user){
        userDao.add(user);
    }

    @NotNull(message = "User对象不能为Null")
    public void update(User user){
        userDao.update(user);
    }
}
