package com.boss.dai.bean;

import lombok.extern.slf4j.Slf4j;

import javax.websocket.*;
import java.io.IOException;

/**
 * @author 戴若汐
 */

@ClientEndpoint()
@Slf4j
public class Client {
    @OnOpen
    public void onOpen(Session session) {
    }
    @OnMessage
    public void onMessage(Session session,String message){
        log.info("Client onMessage: " + message);
    }

    @OnClose
    public void onClose() {}

    private static void sendMsg(Session session,String msg) throws IOException {
        session.getBasicRemote().sendText(msg);
    }
}
