package com.boss.dai.bean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 戴若汐
 */
@Component
@Slf4j
@ServerEndpoint("/websocket")
public class WebSocketServer {

    /**
     * 存放所有的在线客户端
     */
    private static Map<String, Session> clients = new ConcurrentHashMap<>();

    /**
     * 客户端与服务端连接时触发执行事件
     */
    @OnOpen
    public void onOpen(Session session){
        log.info("有新的客户端连接进来了");
        clients.put(session.getId(), session);
    }

    /**
     * 向客户端发送字符串信息
     */
    private static void sendMsg(Session session, String msg) throws IOException {
        session.getBasicRemote().sendText(msg);
    }

    /**
     * 接收到消息后的处理方式，其中包含客户端的普通信息和心跳包信息，
     * 简单区别处理
     */
    @OnMessage
    public void onMessage(Session session, String msg) throws InterruptedException, IOException{

        if (!"keepalive".equals(msg)) {
            log.info("服务端收到消息：" + msg);
            Thread.sleep(3000L);
            sendMsg(session, msg);
        } else {
            log.info("心跳维护包:" + msg);
        }
    }

    @OnClose
    public void onClose(Session session) {
        log.warn("有客户端断开连接了，断开客户为：" + session.getId());
        clients.remove(session.getId());
    }

    @OnError
    public void onError(Throwable throwable) {
       log.error("服务端出现错误 "+throwable.toString());
    }
}
