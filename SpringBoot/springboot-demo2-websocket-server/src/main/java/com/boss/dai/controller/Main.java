package com.boss.dai.controller;

import com.boss.dai.bean.Client;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;

/**
 * @author 戴若汐
 */
@Slf4j
public class Main {
    /**
     *服务器地址
     */
    private static final String URI_STR = "ws://localhost:8088/serverTest";
    private static Session session;
    /**
     *消息发送事件
     */
    private static long date;
    /**
     *连接状态
     */
    private boolean running=false;

    private void start() {
        WebSocketContainer container = null;
        try {
            container = ContainerProvider.getWebSocketContainer();
        } catch (Exception ex) {
            log.error(ex.toString());
        }
        try {
            URI r = URI.create(URI_STR);
            if (null != container){
                session = container.connectToServer(Client.class, r);
            }
        } catch (DeploymentException | IOException e) {
            log.error(e.toString());
        }
    }
    public static void aciton() {
        Main client = new Main();
        client.start();
        new Thread(client.new KeepAlive()).start();
        String input = "";
        try {

            for (int i = 0; i < 5; i++) {
                /**
                 *注意：此处对session做了同步处理，
                 * 因为下文中发送心跳包也是用的此session,
                 * 不用synchronized做同步处理会报
                 * Exception in thread "Thread-5" java.lang.IllegalStateException: The remote endpoint was in state [TEXT_FULL_WRITING] which is an invalid state for called method
                 * 错误
                 */
                synchronized (session){
                    Main.session.getBasicRemote().sendText("javaclient");
                }
                date = System.currentTimeMillis();
                Thread.sleep(3000L);
            }

        } catch (Exception e) {
            log.error(e.toString());
        }
    }
    /**
     *内部类，用来客户端给服务单发送心跳包维持连接
     */
    class KeepAlive implements Runnable{

        @Override
        public void run() {
            while (true){
                if (System.currentTimeMillis()-date>5000L){
                    try {
                        log.info("发送心跳包");
                        synchronized (session){
                            Main.session.getBasicRemote().sendText("keepalive");
                        }
                        date = System.currentTimeMillis();
                    } catch (IOException e) {
                        log.error(e.toString());
                    }
                }else {
                    try {
                        Thread.sleep(3000L);
                        break;
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public KeepAlive() { }
    }
}
