package com.boss.dai.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 戴若汐
 */
@Slf4j
@RestController
@RequestMapping("/socket")
public class SocketController {
    //访问此方法建立websocket连接。
    @RequestMapping("/action")
    public void actionSocket(){
        Main.aciton();
    }

    @RequestMapping("/test")
    public void actionTest(){
        log.info("测试连通性");
    }
}
