package com.boss.dai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo3ClientApplication.class, args);
    }

}
