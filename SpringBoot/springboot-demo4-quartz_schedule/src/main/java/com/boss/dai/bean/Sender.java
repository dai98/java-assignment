package com.boss.dai.bean;

import com.boss.dai.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author 戴若汐
 */
@Component
public class Sender {
    private int i = 1;

    @Autowired
    MailService mailService;

    @Scheduled(cron = "0 0/30 * * * ?")
    public void send(){
        String target = "2282581087@qq.com";
        mailService.sentMail(target,"第"+(i++)+"封邮件","第"+(i++)+"封邮件");
    }
}
