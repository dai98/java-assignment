package com.boss.dai.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient

public class SpringcloudDemoConfigClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemoConfigClientApplication.class, args);
    }

}
