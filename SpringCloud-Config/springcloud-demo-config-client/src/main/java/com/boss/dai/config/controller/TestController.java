package com.boss.dai.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 戴若汐
 */

@RestController
@RequestMapping("config")
@RefreshScope
public class TestController {

    @Value("${message}")
    String message;
    @RequestMapping("/test")
    public String testConfig(){
        return message;
    }
}
