package com.boss.dai.eureka;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan
@EnableDiscoveryClient
public class SpringcloudEurekaDemoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudEurekaDemoBackendApplication.class, args);
    }

}
