package com.boss.dai.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 戴若汐
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SpringcloudEurekaDemoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudEurekaDemoClientApplication.class, args);
    }

}
