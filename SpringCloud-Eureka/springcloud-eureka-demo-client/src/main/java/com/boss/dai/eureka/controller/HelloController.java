package com.boss.dai.eureka.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 戴若汐
 */
@RequestMapping("/demo")
@RestController
public class HelloController {

    @Value("${server.port}")
    String port;
    @RequestMapping("/sayHello")
    public String sayHello(@RequestParam String name){
        return "Hello, My name is " + name + ", I'm from Port " + port;
    }
}
