package com.boss.dai.feign;

import com.boss.dai.feign.service.FeignService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author 戴若汐
 */
@SpringBootApplication
@EnableFeignClients(clients = FeignService.class)
public class SpringcloudDemoBackendFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemoBackendFeignApplication.class, args);
    }

    @Bean
    @LoadBalanced
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
