package com.boss.dai.hystrix.controller;

import com.boss.dai.hystrix.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 戴若汐
 */
@RestController
public class HelloController {

    @Autowired
    HelloService helloService;

    @RequestMapping(value="/hi")
    public String hi(@RequestParam String name){
        return helloService.hiService(name);
    }
}
