package com.boss.dai.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudDemoBackendZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemoBackendZuulApplication.class, args);
    }

}
