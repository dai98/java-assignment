package com.boss.dai.backend.controller;


import com.boss.dai.backend.bean.Student;
import com.boss.dai.backend.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author 戴若汐
 */
@RestController
public class FeignController implements FeignService {

    @Autowired
    FeignService feignService;

    @Override
    public Map<String, Object> findAll() {
        return feignService.findAll();
    }

    @Override
    public Map<String, Object> add(Student student) {
        return feignService.add(student);
    }

    @Override
    public Map<String, Object> delete(String id) {
        return feignService.delete(id);
    }

    @Override
    public Student findOne(String id) {
        return feignService.findOne(id);
    }

    @Override
    public Map<String, Object> update(Student student) {
        return feignService.update(student);
    }
}
