package com.boss.dai.backend.service;

import com.boss.dai.backend.bean.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Map;

/**
 * @author 戴若汐
 */
@FeignClient(name = "vue", fallback = FeignServiceHystrix.class)
@Service
public interface FeignService {

    @GetMapping("findAll")
    Map<String,Object> findAll();

    @PostMapping("add")
    Map<String,Object> add(@RequestBody Student student);

    @GetMapping("delete")
    Map<String, Object> delete(String id);

    @GetMapping("findOne")
    Student findOne(String id);

    @GetMapping("update")
    Map<String, Object> update(@RequestBody Student student);
}
