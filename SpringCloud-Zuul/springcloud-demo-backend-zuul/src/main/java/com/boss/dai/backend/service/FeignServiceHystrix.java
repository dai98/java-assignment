package com.boss.dai.backend.service;

import com.boss.dai.backend.bean.Student;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author 戴若汐
 */
@Slf4j
public class FeignServiceHystrix implements FeignService {
    private static final String ERROR_LOG = "Error!";
    @Override
    public Map<String, Object> findAll() {
        log.error(ERROR_LOG);
        return null;
    }

    @Override
    public Map<String, Object> add(Student student) {
        log.error(ERROR_LOG);
        return null;
    }

    @Override
    public Map<String, Object> delete(String id) {
        log.error(ERROR_LOG);
        return null;
    }

    @Override
    public Student findOne(String id) {
        log.error(ERROR_LOG);
        return null;
    }

    @Override
    public Map<String, Object> update(Student student) {
        log.error(ERROR_LOG);
        return null;
    }
}
