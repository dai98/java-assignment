package com.boss.dai.backend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan
@EnableDiscoveryClient
@EnableFeignClients
public class SpringcloudDemoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemoBackendApplication.class, args);
    }

}
