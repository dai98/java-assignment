package com.boss.dai.backend.dao;


import com.boss.dai.backend.bean.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 戴若汐
 */

@Mapper
@Repository
public interface StudentDao {

    /**
     * 保存学生信息
     * @param student 学生对象
     */
    void save(Student student);

    /**
     * 更新学生信息
     * @param student 学生对象
     */
    void update(Student student);

    /**
     * 删除学生信息
     * @param id 学生id
     */
    void delete(String id);

    /**
     * 返回所有学生
     * @return 学生数组
     */
    List<Student> findAll();

    /**
     * 通过Id查找学生
     * @param id 学生id
     * @return 学生对象
     */
    Student findById(String id);
}
