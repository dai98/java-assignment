package com.boss.dai.backend.service.impl;

import com.boss.dai.backend.bean.Student;
import com.boss.dai.backend.dao.StudentDao;
import com.boss.dai.backend.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 戴若汐
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;
    /**
     * 保存学生信息
     *
     * @param student 学生对象
     */
    @Override
    public void save(Student student) {
        studentDao.save(student);
    }

    /**
     * 更新学生信息
     *
     * @param student 学生对象
     */
    @Override
    public void update(Student student) {
        studentDao.update(student);
    }

    /**
     * 删除学生信息
     *
     * @param id 学生id
     */
    @Override
    public void delete(String id) {
        studentDao.delete(id);
    }

    /**
     * 返回所有学生
     *
     * @return 学生数组
     */
    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    /**
     * 通过Id查找学生
     *
     * @param id 学生id
     * @return 学生对象
     */
    @Override
    public Student findById(String id) {
        return studentDao.findById(id);
    }
}
