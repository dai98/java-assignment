package com.boss.dai.springmvc.controller;

import com.boss.dai.springmvc.model.Student;
import org.springframework.web.bind.annotation.*;

/**
 * @author 戴若汐
 */
@RestController
@RequestMapping("/params")
public class ParamsController {

    String[] prefix = new String[]{"id: "," name: "," status: "};

    @RequestMapping("/params1")
    @GetMapping("/getMapping")
    public String params1(Integer id,String name,Boolean status){
        return prefix[0] + id + prefix[1]  + name + prefix[2] + status;
    }

    @RequestMapping("/params2")
    public String params2(Integer id,String name,boolean status){
        return prefix[0] + id + prefix[1]  + name + prefix[2] + status;
    }

    @RequestMapping("/params3")
    public String params3(@RequestParam(value = "number",required = true) Integer id){
        return prefix[0] + id;
    }

    @RequestMapping("/params4/{name}")
    @PostMapping("/postMapping")
    public String params4(@PathVariable("name") String str,
                          @CookieValue("Webstorm-f1dd0805") String cookie,
                          @RequestHeader("Accept-Language") String header){

        return "str:"+str+" cookie:"+cookie+" header:"+header;
    }

    @RequestMapping("/params5")
    public String params5(Student student){
        return prefix[0] + student.getId() + prefix[1]  + student.getName() + " age: " + student.getAge();
    }
}
