package com.boss.dai.springmvc.service;


import com.boss.dai.springmvc.dao.StudentDao;
import com.boss.dai.springmvc.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentDao studentDao;

    public List<Student> findAll(){
        return studentDao.findAll();
    }

    public int add(Student student){
        return studentDao.add(student);
    }

    public Student findById(int id){
        return studentDao.findById(id);
    }

    public int update(Student student){
        return studentDao.update(student);
    }

    public int delete(int id){
        return studentDao.delete(id);
    }
}
