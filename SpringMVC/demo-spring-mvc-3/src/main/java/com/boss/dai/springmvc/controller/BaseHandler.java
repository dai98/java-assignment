package com.boss.dai.springmvc.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 戴若汐
 */
@Slf4j
public class BaseHandler {

    @ExceptionHandler
    public @ResponseBody String exceptionHandler(Exception exception, HttpServletResponse response) {
        log.error(exception.toString());
        return "系统发生错误";
    }
}
