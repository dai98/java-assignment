package com.boss.dai.springmvc.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/params")
public class ParamsController extends BaseHandler{

    @RequestMapping("/param1")
    @GetMapping("/getMapping")
    public String params1(Integer id,String name,Boolean status){
        return "id:"+id+" name:"+name+" status:"+status;
    }
}
