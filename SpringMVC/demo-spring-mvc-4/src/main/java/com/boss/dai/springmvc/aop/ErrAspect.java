package com.boss.dai.springmvc.aop;

import com.boss.dai.springmvc.exception.Thrower;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author 戴若汐
 */
@Aspect
@Component
public class ErrAspect {

    @Pointcut("execution(* com.boss.dai.springmvc.*.*(..))")
    public void pointcut(){
        /**
         * Pointcut
         */
    }

    @AfterThrowing(pointcut = "pointcut()",throwing = "e")
    public void err(JoinPoint joinPoint, Exception e){
        Thrower.throwErr(e);
    }
}
