package com.boss.dai.springmvc.controller;


import com.boss.dai.springmvc.model.Student;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/params")
public class ParamsController{

    @RequestMapping("/params1")
    @GetMapping("/getMapping")
    public String params1(Integer id,String name,Boolean status){
        return "id:"+id+" name:"+name+" status:"+status;
    }

    @RequestMapping("/params2")
    public String params1(int id,String name,boolean status){
        return "id:"+id+" name:"+name+" status:"+status;
    }

    @RequestMapping("/params3")
    public String params3(@RequestParam(value = "number",required = true) Integer id){
        return "id:"+id;
    }

    @RequestMapping("/params4/{name}")
    @PostMapping("/postMapping")
    public String params4(@PathVariable("name") String str,
                          @CookieValue("Webstorm-f1dd0805") String cookie,
                          @RequestHeader("Accept-Language") String header){

        return "str:"+str+" cookie:"+cookie+" header:"+header;
    }

    @RequestMapping("/params5")
    public String params5(Student student){
        return "id:"+student.getId()+" name:"+student.getName()+" age:"+student.getAge();
    }
}
