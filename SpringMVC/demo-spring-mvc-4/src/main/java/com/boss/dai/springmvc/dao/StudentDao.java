package com.boss.dai.springmvc.dao;

import com.boss.dai.springmvc.exception.MyThrowsException;
import com.boss.dai.springmvc.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class StudentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    //查询所有学生
    public List<Student> findAll(){
        String sql = "select * from tb_student";
        return jdbcTemplate.query(sql,new StudentRowMapper());
    }
    //学生添加
    public int add(Student student){
        if(student == null){
            MyThrowsException.throwErr(new NullPointerException());
        }
        String sql = "insert tb_student(name,age) values(?,?)";
        return jdbcTemplate.update(sql,new Object []{student.getName(),student.getAge()});
    }
    //通过id查询学生
    public Student findById(int id){
        String sql = "select * from tb_student where id = ?";
        Student student = null;
        //如果查询结果为空，会抛出EmptyResultDataAccessException异常
        try {
            student = jdbcTemplate.queryForObject(sql,new Object []{id},new StudentRowMapper());
        }catch (EmptyResultDataAccessException e){
            return null;
        }
        return student;
    }
    //学生修改
    public int update(Student student){
        if(student == null){
            MyThrowsException.throwErr(new NullPointerException());
        }
        String sql = "update tb_student set name = ? , age = ? where id =?";
        return jdbcTemplate.update(sql,new Object [] {student.getName(),student.getAge(),student.getId()});
    }
    //学生删除
    public int delete(int id){
        String sql = "delete from tb_student where id = ?";
        return jdbcTemplate.update(sql,new Object[]{id});
    }
    class StudentRowMapper implements RowMapper<Student>{
        @Override
        public Student mapRow(ResultSet resultSet, int i) throws SQLException {
            Student student = new Student();
            student.setId(resultSet.getInt("id"));
            student.setName(resultSet.getString("name"));
            student.setAge(resultSet.getInt("age"));
            return student;
        }
    }
}
