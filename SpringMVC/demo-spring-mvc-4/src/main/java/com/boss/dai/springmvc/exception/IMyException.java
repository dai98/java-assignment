package com.boss.dai.springmvc.exception;

public interface IMyException {
    int code();
    String msg();
}
