package com.boss.dai.springmvc.exception;

public enum MyExceptionCode implements IMyException {

    /**
     * 未知错误
     */
    UNKNOWN_EXCEPION(-1,"未知错误"),
    /**
     * 用户年龄超出范围
     */
    USER_AGE_SCOPE_EXCEPTION(2001,"用户年龄范围超出异常"),
    /**
     * 包含非法字符
     */
    USER_NAME_EXCEPTION(2002,"用户名字包含非法字符");

    // 代码
    private int code;
    // 消息
    private String msg;

    private MyExceptionCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String msg() {
        return msg;
    }
}
