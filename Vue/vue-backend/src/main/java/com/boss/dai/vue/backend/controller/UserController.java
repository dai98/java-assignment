package com.boss.dai.vue.backend.controller;

import com.boss.dai.vue.backend.bean.Student;
import com.boss.dai.vue.backend.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 戴若汐
 */
@RestController
@CrossOrigin
@RequestMapping("student")
@Slf4j
public class UserController {

    @Autowired
    private StudentService studentService;
    private static final String SUCCESS_TOKEN = "success";
    private static final String MSG_TOKEN = "msg";

    /**
     * 查询所有
     */
    @GetMapping("findAll")
    public Map<String,Object> findAll(){
        List<Student> results = studentService.findAll();
        Map<String,Object> map = new HashMap<>(4);

        map.put("total",10);
        map.put("totalPage",1);
        map.put("page",1);
        map.put("results",results);
        return map;
    }

    /**
     * 添加用户
     */
    @PostMapping("add")
    public Map<String,Object> add(@RequestBody Student student){
        Map<String, Object> map = new HashMap<>(2);
        try{
            studentService.save(student);
            map.put(SUCCESS_TOKEN,true);
            map.put(MSG_TOKEN,"添加用户成功");
            return map;
        }catch(Exception e){
            log.error(e.toString());
            map.put(SUCCESS_TOKEN,false);
            map.put(MSG_TOKEN,"添加用户失败 "+e.getMessage());
            return map;
        }
    }

    /**
     * 删除
     */
    @GetMapping("delete")
    public Map<String, Object> delete(String id){
        Map<String, Object> map = new HashMap<>(2);
        try{
            studentService.delete(id);
            map.put(SUCCESS_TOKEN,true);
            map.put(MSG_TOKEN,"删除用户成功");
            return map;
        }catch(Exception e){
            log.error(e.toString());
            map.put(SUCCESS_TOKEN,false);
            map.put(MSG_TOKEN,"删除用户失败 "+e.getMessage());
            return map;
        }
    }

    /**
     * 查询一个用户
     */
    @GetMapping("findOne")
    public Student findOne(String id){
        return studentService.findById(id);
    }

    /**
     * @author 更新用户
     */
    @GetMapping("update")
    public Map<String, Object> update(@RequestBody Student student){
        Map<String, Object> map = new HashMap<>(2);
        try{
            studentService.update(student);
            map.put(SUCCESS_TOKEN,true);
            map.put(MSG_TOKEN,"更新用户成功");
            return map;
        }catch(Exception e){
            log.error(e.toString());
            map.put(SUCCESS_TOKEN,false);
            map.put(MSG_TOKEN,"更新用户失败 "+e.getMessage());
            return map;
        }
    }

}
