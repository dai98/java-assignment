package com.boss.dai.vue.backend.service;

import com.boss.dai.vue.backend.bean.Student;

import java.util.List;

/**
 * @author 戴若汐
 */
public interface StudentService {

    /**
     * 保存学生信息
     * @param student 学生对象
     */
    void save(Student student);

    /**
     * 更新学生信息
     * @param student 学生对象
     */
    void update(Student student);

    /**
     * 删除学生信息
     * @param id 学生id
     */
    void delete(String id);

    /**
     * 返回所有学生
     * @return 学生数组
     */
    List<Student> findAll();

    /**
     * 通过Id查找学生
     * @param id 学生id
     * @return 学生对象
     */
    Student findById(String id);
}
