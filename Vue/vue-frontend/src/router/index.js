import Vue from 'vue'
import Router from 'vue-router'
import home from "@/components/home"
import student from "@/components/student"
import studentAdd from "@/components/studentAdd"
import studentEdit from "@/components/studentEdit"

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/',redirect:"/home"},
    {path: '/home',component: home},
    {path: '/student',component: student,
       children:[
         {path:'add', component:studentAdd},
         {path:'edit', component:studentEdit}
       ]
     }
  ]
})
